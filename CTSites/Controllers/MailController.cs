﻿
using CTSites.Model;
using CTSites.Services;
using Microsoft.AspNetCore.Mvc;

namespace CTSites.Controllers
{
    public class MailController : ControllerBase
    {
        private readonly IMailService mailService;

        public MailController(IMailService mailService)
        {
            this.mailService = mailService;
        }
        public  async Task<IActionResult> SendMail(MailRequest request)
        {
            try
            {
                await mailService.SendEmailAsync(request);
                return Ok();
            }
            catch (Exception ex)
            {

                throw;
            }

        }



    }
}
