﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Web;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace CTSites.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {

        private readonly IConfiguration _configuration;

        [HttpPost]
        [Route("OktaCustomerLogin/{userId}")]
        public string OktaCustomerLogin(string userId)
        {
            if (!string.IsNullOrWhiteSpace(userId))
            {
                var connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

                try
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
                    SqlDataAdapter da = new SqlDataAdapter();
                    cmd = new SqlCommand("sp_CheckOktaUserId", conn);
                    cmd.Parameters.Add(new SqlParameter("@OktaUserId", userId));
                    cmd.CommandType = CommandType.StoredProcedure;
                    da.SelectCommand = cmd;
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    string jsonString = string.Empty;
                 
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                    }

                    return jsonString;
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error connecting to the database: {ex.Message}");
                    Console.WriteLine($"StackTrace: {ex.StackTrace}");

                    var innerException = ex.InnerException;
                    while (innerException != null)
                    {
                        Console.WriteLine($"Inner Exception: {innerException.Message}");
                        Console.WriteLine($"Inner Exception StackTrace: {innerException.StackTrace}");
                        innerException = innerException.InnerException;
                    }

                    return "Ok";
                }
            }

            return "Ok";
        }
        public LoginController(IConfiguration configuration)
        {
            _configuration = configuration;

        }

        // OKTA LOGIN METHOD

        /*[HttpGet]
        [Route("sso1")]
        public IActionResult Sso(string ticket)
        {
            var SGID = string.Empty;
            var UserID = string.Empty;
            var FirstName = string.Empty;
            var LastName = string.Empty;
            var Email = string.Empty;
            var CompanyName = string.Empty;
            var Country = string.Empty;
            var SecurityLevel = string.Empty;
            var URL = string.Empty;
            var SiteName = string.Empty;
            var SiteId = string.Empty;
            var service = "https://digital-team-805wb.certainteed.com/ProductCTAPI/api/Login/sso1/";
            if (!string.IsNullOrWhiteSpace(ticket))
            {
                // Retrieve SGId from CAS server
                //var sgId = RetrieveSGIdFromTicket(ticket, Request.RequestUri.GetLeftPart(UriPartial.Path));
               // RetrieveSGIdFromTicket(ticket, Request.Scheme + "://" + Request.Host.Value + Request.Path.Value);
                 var sgId = RetrieveSGIdFromTicket(ticket, service);
             //   var sgId = "A9548992";

                if (!string.IsNullOrWhiteSpace(sgId.ToString()))
                {
                    //var connectionString = ConfigurationManager.ConnectionStrings["CTCustomization"]?.ConnectionString;
                    var connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

                    if (string.IsNullOrWhiteSpace(connectionString)) { }

                    try
                    {
                        using (var connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            Console.WriteLine("Connection opened successfully.");

                            using (var command = new SqlCommand("sp_CheckSso1", connection))
                            {
                                command.CommandType = CommandType.StoredProcedure;
                                command.Parameters.Add(new SqlParameter("@SGID", SqlDbType.NVarChar, 50) { Value = sgId });

                                SqlDataReader da = command.ExecuteReader();
                                string jsonString = string.Empty;
                                DataTable dt = new DataTable();
                                dt.Load(da);
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    if (dt.Rows.Count > 1)
                                    {
                                        SGID = sgId.ToString();
                                        UserID = dt.Rows[0]["UserID"].ToString();
                                        FirstName = dt.Rows[0]["FirstName"].ToString();
                                        LastName = dt.Rows[0]["LastName"].ToString();
                                        Email = dt.Rows[0]["EmailAddress"].ToString();
                                        CompanyName = dt.Rows[0]["CompanyName"].ToString();
                                        Country = dt.Rows[0]["Country"].ToString();
                                        SecurityLevel = (string)dt.Rows[0]["SecurityLevel"];

                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {
                                            SiteName = SiteName + dt.Rows[i]["SiteName"].ToString() + ",";
                                            SiteId = SiteId + dt.Rows[i]["SiteId"].ToString() + ",";
                                        }

                                        SiteName = SiteName.TrimEnd(',');
                                        SiteId = SiteId.TrimEnd(',');
                                        //URL = $"http://localhost:4200/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                        URL = $"https://digital-team-805wb.certainteed.com/Productcustomization/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                    }
                                    else
                                    {
                                        SGID = sgId.ToString();
                                        UserID = dt.Rows[0]["UserID"].ToString();
                                        FirstName = dt.Rows[0]["FirstName"].ToString();
                                        LastName = dt.Rows[0]["LastName"].ToString();
                                        Email = dt.Rows[0]["EmailAddress"].ToString();
                                        CompanyName = dt.Rows[0]["CompanyName"].ToString();
                                        Country = dt.Rows[0]["Country"].ToString();
                                        SecurityLevel = (string)dt.Rows[0]["SecurityLevel"];
                                        SiteName = dt.Rows[0]["SiteName"].ToString();
                                        SiteId = dt.Rows[0]["SiteId"].ToString();
                                        //URL = $"http://localhost:4200/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                        URL = $"https://digital-team-805wb.certainteed.com/Productcustomization/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                    }
                                }

                            }
                        }

                        return Redirect(URL);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error connecting to the database: {ex.Message}");
                        Console.WriteLine($"StackTrace: {ex.StackTrace}");

                        var innerException = ex.InnerException;
                        while (innerException != null)
                        {
                            Console.WriteLine($"Inner Exception: {innerException.Message}");
                            Console.WriteLine($"Inner Exception StackTrace: {innerException.StackTrace}");
                            innerException = innerException.InnerException;
                        }

                        return Redirect(URL);
                    }
                }
            }

            return Redirect(URL);
        }*/

        [HttpGet]
        [Route("sso1")]
        public IActionResult Sso(string ticket)
        {
            var SGID = string.Empty;
            var UserID = string.Empty;
            var FirstName = string.Empty;
            var LastName = string.Empty;
            var Email = string.Empty;
            var CompanyName = string.Empty;
            var Country = string.Empty;
            var SecurityLevel = string.Empty;
            var URL = string.Empty;
            var SiteName = string.Empty;
            var SiteId = string.Empty;
            //var service = "https://digital-team-805wb.certainteed.com/ProductCTAPI/api/Login/sso1/";
            if (!string.IsNullOrWhiteSpace(ticket))
            {
                // Retrieve SGId from CAS server
                 //var sgId = RetrieveSGIdFromTicket(ticket, Request.RequestUri.GetLeftPart(UriPartial.Path));
                 var sgId = RetrieveSGIdFromTicket(ticket, Request.Scheme + "://" + Request.Host.Value + Request.Path.Value);
                 //var sgId = RetrieveSGIdFromTicket(ticket, service);
                   //var sgId = "A9548993";

                if (!string.IsNullOrWhiteSpace(sgId.ToString()))
                {
                    //var connectionString = ConfigurationManager.ConnectionStrings["CTCustomization"]?.ConnectionString;
                    var connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

                    if (string.IsNullOrWhiteSpace(connectionString)) { }

                    try
                    {
                        using (var connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            Console.WriteLine("Connection opened successfully.");

                            using (var command = new SqlCommand("sp_CheckSso1", connection))
                            {
                                command.CommandType = CommandType.StoredProcedure;
                                command.Parameters.Add(new SqlParameter("@SGID", SqlDbType.NVarChar, 50) { Value = sgId });

                                SqlDataReader da = command.ExecuteReader();
                                string jsonString = string.Empty;
                                DataTable dt = new DataTable();
                                dt.Load(da);
                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    if (dt.Rows.Count > 1)
                                    {
                                        SGID = sgId.ToString();
                                        UserID = dt.Rows[0]["UserID"].ToString();
                                        FirstName = dt.Rows[0]["FirstName"].ToString();
                                        LastName = dt.Rows[0]["LastName"].ToString();
                                        Email = dt.Rows[0]["EmailAddress"].ToString();
                                        CompanyName = dt.Rows[0]["CompanyName"].ToString();
                                        Country = dt.Rows[0]["Country"].ToString();
                                        SecurityLevel = (string)dt.Rows[0]["SecurityLevel"];

                                        for (int i = 0; i < dt.Rows.Count; i++)
                                        {
                                            SiteName = SiteName + dt.Rows[i]["SiteName"].ToString() + ",";
                                            SiteId = SiteId + dt.Rows[i]["SiteId"].ToString() + ",";
                                        }

                                        SiteName = SiteName.TrimEnd(',');
                                        SiteId = SiteId.TrimEnd(',');
                                       URL = $"http://localhost:4200/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                       //URL = $"https://digital-team-805wb.certainteed.com/Productcustomization/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                    }
                                    else
                                    {
                                        SGID = sgId.ToString();
                                        UserID = dt.Rows[0]["UserID"].ToString();
                                        FirstName = dt.Rows[0]["FirstName"].ToString();
                                        LastName = dt.Rows[0]["LastName"].ToString();
                                        Email = dt.Rows[0]["EmailAddress"].ToString();
                                        CompanyName = dt.Rows[0]["CompanyName"].ToString();
                                        Country = dt.Rows[0]["Country"].ToString();
                                        SecurityLevel = (string)dt.Rows[0]["SecurityLevel"];
                                        SiteName = dt.Rows[0]["SiteName"].ToString();
                                        SiteId = dt.Rows[0]["SiteId"].ToString();
                                        URL = $"http://localhost:4200/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                        //URL = $"https://digital-team-805wb.certainteed.com/Productcustomization/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                    }
                                }

                            }
                        }

                        return Redirect(URL);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine($"Error connecting to the database: {ex.Message}");
                        Console.WriteLine($"StackTrace: {ex.StackTrace}");

                        var innerException = ex.InnerException;
                        while (innerException != null)
                        {
                            Console.WriteLine($"Inner Exception: {innerException.Message}");
                            Console.WriteLine($"Inner Exception StackTrace: {innerException.StackTrace}");
                            innerException = innerException.InnerException;
                        }

                        return Redirect(URL);
                    }
                }
            }

            return Redirect(URL);
        }
        private string RetrieveSGIdFromTicket(string ticket, string service)
        {
            using (var webClient = new WebClient())
            {
                var validateUrl = "https://uat.websso.saint-gobain.com/cas/validate?ticket=" + ticket + "&service=" + service;
                //  var validateUrl = "https://websso.saint-gobain.com/cas/validate?ticket=" + ticket + "&service=" + service;
                var stream = webClient.OpenRead(validateUrl);
                if (stream != null)
                {
                    var reader = new StreamReader(stream);
                    var response = reader.ReadToEnd();
                    var ssoResult = response.Split(new[] { '\n' }, StringSplitOptions.None);
                    if (ssoResult[0].Equals("yes", StringComparison.InvariantCultureIgnoreCase))
                    {
                        return ssoResult[1];
                    }
                }
            }
            return null;
        }

        [HttpGet]
        [Route("sso2")]
        public async Task<IActionResult> Sso1(string ticket)
        {
            var token = string.Empty;
            var service = Request.Scheme + "://" + Request.Host.Value + Request.Path.Value;
            var SGID = string.Empty;
            var UserID = string.Empty;
            var FirstName = string.Empty;
            var LastName = string.Empty;
            var Email = string.Empty;
            var CompanyName = string.Empty;
            var Country = string.Empty;
            var SecurityLevel = string.Empty;
            var URL = string.Empty;
            var SiteName = string.Empty;
            var SiteId = string.Empty;
            //  if (!string.IsNullOrWhiteSpace(ticket))
            //  {
            // Retrieve SGId from CAS server
            //Task<string> SGID = RetrieveSGIdFromTicketAsync(ticket, Request.Scheme + "://" + Request.Host.Value + Request.Path.Value);

            //   }
            // return Request.Scheme + "://" + Request.Host.Value + Request.Path.Value+SGID;
            //return Redirect(Settings.Default.WebApplicationUrl + "forbidden");
            var validateUrl = "https://uat.websso.saint-gobain.com/cas/validate?ticket=" + ticket + "&service=" + service;
            HttpClient client = new HttpClient();
            //return null;
            HttpResponseMessage response = await client.GetAsync(validateUrl);
            response.EnsureSuccessStatusCode();
            var contents = response.Content.ReadAsStringAsync().Result;
            string[] lines = contents.Split(
    new string[] { "\n" },
    StringSplitOptions.None);
            SGID = lines[1];
            if (!string.IsNullOrWhiteSpace(SGID.ToString()))
            {
                //var connectionString = ConfigurationManager.ConnectionStrings["CTCustomization"]?.ConnectionString;
                var connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

                if (string.IsNullOrWhiteSpace(connectionString)) { }

                try
                {
                    using (var connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        Console.WriteLine("Connection opened successfully.");

                        using (var command = new SqlCommand("sp_CheckSso1", connection))
                        {
                            command.CommandType = CommandType.StoredProcedure;
                            command.Parameters.Add(new SqlParameter("@SGID", SqlDbType.NVarChar, 50) { Value = SGID });

                            SqlDataReader da = command.ExecuteReader();
                            string jsonString = string.Empty;
                            DataTable dt = new DataTable();
                            dt.Load(da);
                            if (dt != null && dt.Rows.Count > 0)
                            {
                                if (dt.Rows.Count > 1)
                                {
                                    SGID = SGID.ToString();
                                    UserID = dt.Rows[0]["UserID"].ToString();
                                    FirstName = dt.Rows[0]["FirstName"].ToString();
                                    LastName = dt.Rows[0]["LastName"].ToString();
                                    Email = dt.Rows[0]["EmailAddress"].ToString();
                                    CompanyName = dt.Rows[0]["CompanyName"].ToString();
                                    Country = dt.Rows[0]["Country"].ToString();
                                    SecurityLevel = (string)dt.Rows[0]["SecurityLevel"];

                                    for (int i = 0; i < dt.Rows.Count; i++)
                                    {
                                        SiteName = SiteName + dt.Rows[i]["SiteName"].ToString() + ",";
                                        SiteId = SiteId + dt.Rows[i]["SiteId"].ToString() + ",";
                                    }

                                    SiteName = SiteName.TrimEnd(',');
                                    SiteId = SiteId.TrimEnd(',');
                                    //URL = $"http://localhost:4200/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                    URL = $"https://digital-team-805wb.certainteed.com/Productcustomization/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                }
                                else
                                {
                                    SGID = SGID.ToString();
                                    UserID = dt.Rows[0]["UserID"].ToString();
                                    FirstName = dt.Rows[0]["FirstName"].ToString();
                                    LastName = dt.Rows[0]["LastName"].ToString();
                                    Email = dt.Rows[0]["EmailAddress"].ToString();
                                    CompanyName = dt.Rows[0]["CompanyName"].ToString();
                                    Country = dt.Rows[0]["Country"].ToString();
                                    SecurityLevel = (string)dt.Rows[0]["SecurityLevel"];
                                    SiteName = dt.Rows[0]["SiteName"].ToString();
                                    SiteId = dt.Rows[0]["SiteId"].ToString();
                                    URL = $"http://localhost:4200/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                   // URL = $"https://digital-team-805wb.certainteed.com/Productcustomization/authorize?SGID={SGID}&FirstName={FirstName}&LastName={LastName}&Email={Email}&CompanyName={CompanyName}&Country={Country}&SecurityLevel={SecurityLevel}&UserID={UserID}&SiteName={SiteName}&SiteId={SiteId}";
                                }
                            }

                        }
                    }

                    return Redirect(URL);
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error connecting to the database: {ex.Message}");
                    Console.WriteLine($"StackTrace: {ex.StackTrace}");

                    var innerException = ex.InnerException;
                    while (innerException != null)
                    {
                        Console.WriteLine($"Inner Exception: {innerException.Message}");
                        Console.WriteLine($"Inner Exception StackTrace: {innerException.StackTrace}");
                        innerException = innerException.InnerException;
                    }

                    return Redirect(URL);
                }
                return Redirect(URL);
            }
            return Redirect(URL);
        }
        [HttpGet]
        [Route("sso")]
        public string GetPage(string ticket)
        {
            var SGID = string.Empty;
            var validateUrl = string.Empty;
            //HttpClient client = new HttpClient();
            var service = "https://digital-team-805wb.certainteed.com/ProductCTAPI/api/Login/sso1/";//Request.Scheme + "://" + Request.Host.Value + Request.Path.Value;
            Console.WriteLine(service);
            //var validateUrl = "https://uat.websso.saint-gobain.com/cas/validate?ticket=" + ticket + "&service=" + service;
            //HttpResponseMessage response = client.GetAsync(validateUrl).Result;

            //if (response.IsSuccessStatusCode)
            //{
            //    string page = response.Content.ReadAsStringAsync().Result;
            //    return page+"Successfully load page";
            //}
            //else
            //{
            //    return "Invalid Page url requested";
            //}


            using (var webClient = new WebClient())
            {
                   validateUrl = "https://uat.websso.saint-gobain.com/cas/validate?ticket=" + ticket + "&service=" + service;
              //  var validateUrl = "https://websso.saint-gobain.com/cas/validate?ticket=" + ticket + "&service=" + service;
                var stream = webClient.OpenRead(validateUrl);
                if (stream != null)
                {
                    var reader = new StreamReader(stream);
                    var response = reader.ReadToEnd();
                    SGID = response;
                        //var ssoResult = response.Split(new[] { '\n' }, StringSplitOptions.None);
                        //if (ssoResult[0].Equals("yes", StringComparison.InvariantCultureIgnoreCase))
                        //{
                        //SGID= ssoResult[1];
                        //}
                   
                }
            }
            return validateUrl+" "+service+" " + SGID;
        }


    }
    }
