﻿using CTSites.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Data;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using MailKit.Net.Smtp;

using MailKit.Security;
using MimeKit;
using CTSites.Services;
using System;

namespace CTSites.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        public readonly IConfiguration _configuration;
        private readonly IMailService mailService;
        private readonly IWebHostEnvironment _environment; // for file upload
        //private readonly string _imagesFolderPath = "Resources\\Images\\"; // local location
        private readonly string _imagesFolderPath = "D:/Websites/productcustomization.certainteed.com/API/files/";
        //private readonly string _proofsFolderPath = "Files\\Proof\\"; // local location
        private readonly string _proofsFolderPath = "D:/Websites/productcustomization.certainteed.com/API/files/";
        private Exception? message;

        public OrdersController(IConfiguration configuration, IMailService mailService, IWebHostEnvironment environment)
        {
            _configuration = configuration;
            this.mailService = mailService;
            _environment = environment;
        }

        [HttpGet]
        [Route("GetQuickActionOrders")]
        public string GetQuickActionOrders(string SiteName)
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
        conn.Open();
            var command = new SqlCommand("sp_GetQuickActionOrders", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SiteName", SiteName);

            SqlDataReader da = command.ExecuteReader();
            string jsonString = string.Empty;
            DataTable dt = new DataTable();
            dt.Load(da);
            conn.Close();
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }

            return jsonString;
        }

        [HttpGet]
        [Route("SendEmail")]
        public string SendEmailAsync(string TOEmail, string Body, string subject)
        {
            string Message = "";


            MailMessage msg = new MailMessage();
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            msg.From = new MailAddress("DoNotReply@saint-gobain.com", "No-Reply", System.Text.Encoding.UTF8);
            msg.Subject = subject;//Message header 
            msg.SubjectEncoding = Encoding.UTF8;//Mail title code 
            msg.IsBodyHtml = true;

            msg.Body = Body;
            msg.To.Add(TOEmail);
            msg.BodyEncoding = Encoding.UTF8;//Message code 
            msg.IsBodyHtml = true;//Whether the HTML mail 
            msg.Priority = MailPriority.High;//Priority mail
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            //     SmtpServer.Port = 587;
            client.Credentials = new System.Net.NetworkCredential("", "password");
            //client.Host = Get_AppSettings("smtpKey");
            client.Host = "localhost";
            object userState = msg;
            try
            {
                client.Send(msg);
                Message = "success";
                return "success";
                //  return true;
            }
            catch (Exception err)
            {
                Message = err.InnerException.ToString();
                System.Diagnostics.Debug.WriteLine("MAIL ERROR: " + err.Message.ToString());
                return Message;
            }



        }

        [HttpGet]
        [Route("GetUsers")]
        public string GetUsers()
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            SqlDataAdapter da = new SqlDataAdapter("spGetAllUsers", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }

            return jsonString;
        }

        [HttpGet]
        [Route("GetAllCountries")]
        public string GetAllCountries()
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            SqlDataAdapter da = new SqlDataAdapter("spGetAllCountries", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            return jsonString;
        }
        
        [HttpGet]
        [Route("GetAllStates")]
        public string GetAllStates()
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            SqlDataAdapter da = new SqlDataAdapter("spGetAllStates", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            return jsonString;
        }
        
        [HttpGet]
        [Route("GetAllLogsDetails")]
        public string GetAllLogsDetails()
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            SqlDataAdapter da = new SqlDataAdapter("spGetAllLogsDetails", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            return jsonString;
        }

        [HttpGet] // LOGS FOR ACCOUNT HISTORY WITHOUT ORDERID FIELD
        [Route("GetAccountHistory/{userID}")]
        public string GetAccountHistory(int userID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlDataAdapter da = new SqlDataAdapter("spGetAccountHistory", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add UserID parameter
                da.SelectCommand.Parameters.AddWithValue("@UserID", userID);

                DataTable dt = new DataTable();
                string jsonString = string.Empty;
                da.Fill(dt);

                if (dt != null && dt.Rows.Count > 0)
                {
                    jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                }

                return jsonString;
            }
        }

        [HttpGet] // LOGS FOR ORDERS HISTORY WITH ORDERID FIELD
        [Route("GetOrdersHistorywithOrderID/{userID}")]
        public string GetOrdersHistorywithOrderID(int userID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlDataAdapter da = new SqlDataAdapter("spGetOrdersHistorywithOrderID", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add UserID parameter
                da.SelectCommand.Parameters.AddWithValue("@UserID", userID);

                DataTable dt = new DataTable();
                string jsonString = string.Empty;
                da.Fill(dt);

                if (dt != null && dt.Rows.Count > 0)
                {
                    jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                }

                return jsonString;
            }
        }

        [HttpGet] // FOR REPORTS DASHBOARD -> ANNUALLY ORDER -> ADDING INTO CHART.JS
        [Route("GetAnnuallyOrderDetails/{userID}")]
        public string GetAnnuallyOrderDetails(int userID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlDataAdapter da = new SqlDataAdapter("spGetAnnuallyOrderDetails", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add UserID parameter
                da.SelectCommand.Parameters.AddWithValue("@UserID", userID);

                DataTable dt = new DataTable();
                string jsonString = string.Empty;
                da.Fill(dt);

                if (dt != null && dt.Rows.Count > 0)
                {
                    jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                }

                return jsonString;
            }
        }

        [HttpGet] // FOR REPORTS DASHBOARD -> ADDING INTO CHART.JS
        [Route("GetMonthlyOrderDetails/{userID}")]
        public string GetMonthlyOrderDetails(int userID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlDataAdapter da = new SqlDataAdapter("spGetMonthlyOrderDetails", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add UserID parameter
                da.SelectCommand.Parameters.AddWithValue("@UserID", userID);

                DataTable dt = new DataTable();
                string jsonString = string.Empty;
                da.Fill(dt);

                if (dt != null && dt.Rows.Count > 0)
                {
                    jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                }

                return jsonString;
            }
        }

        [HttpGet] // FOR REPORTS DASHBOARD -> ORDERS IN LAST 60 DAYS -> ADDING INTO CHART.JS
        [Route("GetOrdersInLast60Days/{userID}")]
        public string GetOrdersInLast60Days(int userID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlDataAdapter da = new SqlDataAdapter("spGetOrdersInLast60Days", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add UserID parameter
                da.SelectCommand.Parameters.AddWithValue("@UserID", userID);

                DataTable dt = new DataTable();
                string jsonString = string.Empty;
                da.Fill(dt);

                if (dt != null && dt.Rows.Count > 0)
                {
                    jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                }

                return jsonString;
            }
        }


        [HttpGet] // FOR REPORTS DASHBOARD -> ORDERS IN LAST 60 DAYS -> ADDING INTO CHART.JS
        [Route("GetProductLogs/{userID}")]
        public string GetProductLogs(int userID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                SqlDataAdapter da = new SqlDataAdapter("spGetProductLogs", conn);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add UserID parameter
                da.SelectCommand.Parameters.AddWithValue("@UserID", userID);

                DataTable dt = new DataTable();
                string jsonString = string.Empty;
                da.Fill(dt);

                if (dt != null && dt.Rows.Count > 0)
                {
                    jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                }

                return jsonString;
            }
        }



        [HttpGet]
        [Route("GetUserswithId")]
        public string GetUserswithId(int UserID)
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            conn.Open();

            var command = new SqlCommand("spGetUser", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserId", UserID);

            SqlDataReader da = command.ExecuteReader();
            string jsonString = string.Empty;
            DataTable dt = new DataTable();
            dt.Load(da);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            conn.Close();

            return jsonString;
        }


        [HttpGet]
        [Route("GetSecurityLevelByUserId/{UserID}")]
        public IActionResult GetSecurityLevelByUserId(int UserID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            string queryString = "EXEC spGetSecurityLevelByUserId @UserID";
            DataTable dt = new DataTable();
            string jsonString = string.Empty;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    command.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dt);
                    }
                }
            }



            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }
        
        [HttpGet]
        [Route("GetCompletedOrders/{StatusId}")]
        public IActionResult GetCompletedOrders(int StatusId)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            string queryString = "EXEC spGetCompletedOrders @StatusId";
            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    command.Parameters.Add("@StatusId", SqlDbType.Int).Value = StatusId;
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dt);
                    }
                }
            }
            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }
        
        [HttpGet]
        [Route("GetNewOrderLog/{OrderID}")]
        public string GetNewOrderLog(string OrderID)
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            conn.Open();
            var command = new SqlCommand("spGetNewOrderLog", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderID", OrderID);
            SqlDataReader da = command.ExecuteReader();
            string jsonString = string.Empty;
            DataTable dt = new DataTable();
            dt.Load(da);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            conn.Close();
            return jsonString;
        }
        
        [HttpGet]
        [Route("GetFullNameAndEmailAddressByOrderID/{OrderID}")]
        public string GetFullNameAndEmailAddressByOrderID(string OrderID)
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            conn.Open();
            var command = new SqlCommand("spGetFullNameAndEmailAddressByOrderID", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderID", OrderID);
            SqlDataReader da = command.ExecuteReader();
            string jsonString = string.Empty;
            DataTable dt = new DataTable();
            dt.Load(da);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            conn.Close();
            return jsonString;
        }
        
        [HttpGet]
        [Route("CheckCTOrderNumber")]
        public IActionResult CheckCTOrderNumberAvailability(string orderNumber)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization")))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("spCheckCTOrderNumberAvailability", connection);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CTOrderNumber", orderNumber);
                    int count = (int)command.ExecuteScalar();
                    if (count > 0)
                    {
                        return Ok(new { message = "This Order Number is already taken." });
                    }
                    else
                    {
                        return Ok(new { message = "This Order Number is available." });
                    }
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = "An error occurred while checking CTOrderNumber availability: " + ex.Message });
            }
        }
        
        [HttpGet]
        [Route("GetOrderDetailsByOrderID/{OrderID}")]
        public string GetOrderDetailsByOrderID(string OrderID)
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            conn.Open();
            var command = new SqlCommand("spGetOrderDetailsByOrderID", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderID", OrderID);
            SqlDataReader da = command.ExecuteReader();
            string jsonString = string.Empty;
            DataTable dt = new DataTable();
            dt.Load(da);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            conn.Close();
            return jsonString;
        }

        [HttpGet]
        [Route("ViewSelectedOrderSite/{OrderID}")]
        public IActionResult ViewSelectedOrderSite(Guid OrderID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            string queryString = "EXEC spViewSelectedOrderSite @OrderID";
            DataTable dt = new DataTable();
            string jsonString = string.Empty;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    command.Parameters.Add("@OrderID", SqlDbType.UniqueIdentifier).Value = OrderID;
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dt);
                    }
                }
            }

            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("currentOrderStatus/{OrderID}")]
        public IActionResult GetCurrentOrderStatus(Guid OrderID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            string queryString = "EXEC spCurrentOrderStatus @OrderID";
            DataTable dt = new DataTable();
            string jsonString = string.Empty;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    command.Parameters.Add("@OrderID", SqlDbType.UniqueIdentifier).Value = OrderID;
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dt);
                    }
                }
            }

            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("GetOrderDetails/{OrderID}")]
        public IActionResult GetOrderDetails(Guid OrderID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            string queryString = "EXEC spGetOrderDetails @OrderID";
            DataTable dt = new DataTable();
            string jsonString = string.Empty;

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    command.Parameters.Add("@OrderID", SqlDbType.UniqueIdentifier).Value = OrderID;
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dt);
                    }
                }
            }

            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("GetSites/{UserID}")]
        public string GetSites(int UserID)
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            string jsonString = string.Empty;
            conn.Open();
            var command = new SqlCommand("spGetAllSites", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserId", UserID);

            SqlDataReader da = command.ExecuteReader();

            DataTable dt = new DataTable();
            dt.Load(da);

            conn.Close();

            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }

            return jsonString;
        }

        [HttpGet]
        [Route("GetAllSecurityLevels")]
        public string GetAllSecurityLevels()
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));

            SqlDataAdapter da = new SqlDataAdapter("spGetAllSecurityLevels", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;

            DataTable dt = new DataTable();
            string jsonString = string.Empty;

            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }

            return jsonString;
        }

        [HttpGet]
        [Route("GetUserInfoByUserID/{UserID}")]
        public IActionResult GetUserInfoByUserID(int UserID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            string queryString = "EXEC getUserInfoByUserID @UserID";
            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    command.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dt);
                    }
                }
            }
            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }

        /*[HttpGet]
        [Route("GetCustomerEmails")]
        public string GetCustomerEmails(int SiteId)
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
     
            var command = new SqlCommand("spGetAllCustomerEmails", conn);
            conn.Open();
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SiteId", SiteId);

            SqlDataReader da = command.ExecuteReader();
            string jsonString = string.Empty;
            DataTable dt = new DataTable();
            dt.Load(da);
            conn.Close();
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return jsonString;
            }

            return "error";
        }*/
        [HttpGet]
        [Route("GetCustomerEmails")]
        public string GetCustomerEmails(int SiteId)
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            var command = new SqlCommand("spGetAllCustomerEmails", conn);
            conn.Open();
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SiteId", SiteId);

            SqlDataReader da = command.ExecuteReader();
            string jsonString = string.Empty;
            DataTable dt = new DataTable();
            dt.Load(da);
            conn.Close();
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }

            return jsonString;
        }

        /*[HttpGet]
        [Route("GetCustomerEmails")]
        public string GetCustomerEmails(int SiteId)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                conn.Open(); // Open the connection explicitly

                var command = new SqlCommand("spGetAllCustomerEmails", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@SiteId", SiteId);

                using (SqlDataReader da = command.ExecuteReader())
                {
                    DataTable dt = new DataTable();
                    dt.Load(da);

                    if (dt.Rows.Count > 0)
                    {
                        string jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                        return jsonString;
                    }
                }
            }

            return "error";
        }*/


        /*[HttpGet]
        [Route("GetCustomerEmails")]
        public IActionResult GetCustomerEmails(int SiteId)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization")))
                {
                    conn.Open();

                    var command = new SqlCommand("spGetAllCustomerEmails", conn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@SiteId", SiteId);

                    SqlDataReader da = command.ExecuteReader();
                    DataTable dt = new DataTable();
                    dt.Load(da);

                    if (dt != null && dt.Rows.Count > 0)
                    {
                        string jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                        return Ok(jsonString);
                    }
                    else
                    {
                        return NotFound();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return StatusCode(500);
            }
        }*/


        [HttpGet]
        [Route("GetOrderStatus")]
        public string GetOrderStatus()
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            SqlDataAdapter da = new SqlDataAdapter("getAllOrderStatus", conn);
            da.SelectCommand.CommandType = CommandType.StoredProcedure;
            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            da.Fill(dt);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }

            return jsonString;
        }



        [HttpGet]
        [Route("GetQuickActionEndUserOrders")]
        public string GetQuickActionEndUserOrders(int UserId)
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            conn.Open();
            var command = new SqlCommand("sp_GetQuickActionEndUserOrders", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@UserId", UserId);

            SqlDataReader da = command.ExecuteReader();
            string jsonString = string.Empty;
            DataTable dt = new DataTable();
            dt.Load(da);
            conn.Close();
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return jsonString;
            }
            else
            {
                return "NotFound";
            }
        }

        [HttpGet]
        [Route("GetOrders/{SecurityLevel}/{UserId}/{Country}/{SiteName}")]
        public IActionResult GetOrders(int SecurityLevel, int UserId, string Country, string SiteName)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

            string jsonString = string.Empty;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            var command = new SqlCommand("GetAllOrdersbySecurityLevel", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@SecurityLevel", SecurityLevel);
            command.Parameters.AddWithValue("@UserId", UserId);
            command.Parameters.AddWithValue("@Country", Country);
            command.Parameters.AddWithValue("@SiteName", SiteName);
            SqlDataReader da = command.ExecuteReader();

            DataTable dt = new DataTable();
            dt.Load(da);

            connection.Close();
            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }

    

        [HttpGet]
        [Route("ProofApprove/{OrderId}/{UserID}/{IPAddress}/{varProofID}")]
        public IActionResult ProofApprove(string OrderId, int UserID, string IPAddress, string varProofID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

            string jsonString = string.Empty;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            var command = new SqlCommand("spAcceptProof", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderId", OrderId);
            command.Parameters.AddWithValue("@UserID", UserID);
            command.Parameters.AddWithValue("@IPAddress", IPAddress);
            command.Parameters.AddWithValue("@varProofID", varProofID);

            SqlDataReader da = command.ExecuteReader();

            // DataTable dt = new DataTable();
            // dt.Load(da);

            connection.Close();

            jsonString = "Success";
            jsonString = JsonConvert.SerializeObject(jsonString, Formatting.Indented);
            return Ok(jsonString);


        }

        [HttpGet]
        [Route("ProofReject/{OrderId}/{UserID}/{IPAddress}/{varProofID}/{RejectReason}")]
        public IActionResult ProofReject(string OrderId, int UserID, string IPAddress, string varProofID, string RejectReason)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

            string jsonString = string.Empty;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            var command = new SqlCommand("spRejectProof", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderId", OrderId);
            command.Parameters.AddWithValue("@UserID", UserID);
            command.Parameters.AddWithValue("@IPAddress", IPAddress);
            command.Parameters.AddWithValue("@varProofID", varProofID);
            command.Parameters.AddWithValue("@RejectReason", RejectReason);
            SqlDataReader da = command.ExecuteReader();

            // DataTable dt = new DataTable();
            // dt.Load(da);

            connection.Close();

            jsonString = "Success";
            jsonString = JsonConvert.SerializeObject(jsonString, Formatting.Indented);
            return Ok(jsonString);


        }

        [HttpGet]
        [Route("ArtworkReject/{OrderId}/{UserID}/{IPAddress}/{varArtworkID}/{RejectReason}")]
        public IActionResult ArtworkReject(string OrderId, int UserID, string IPAddress, string varArtworkID, string RejectReason)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

            string jsonString = string.Empty;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            var command = new SqlCommand("spRejectArtwork", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderId", OrderId);
            command.Parameters.AddWithValue("@UserID", UserID);
            command.Parameters.AddWithValue("@IPAddress", IPAddress);
            command.Parameters.AddWithValue("@varArtworkID", varArtworkID);
            command.Parameters.AddWithValue("@RejectReason", RejectReason);
            SqlDataReader da = command.ExecuteReader();

            // DataTable dt = new DataTable();
            // dt.Load(da);

            connection.Close();

            jsonString = "Success";
            jsonString = JsonConvert.SerializeObject(jsonString, Formatting.Indented);
            return Ok(jsonString);


        }

        [HttpGet]
        [Route("GetOrderNumberFromUserID/{UserID}")]
        public IActionResult GetOrderNumberFromUserID(int UserID)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            string queryString = "EXEC spGetOrderNumberFromUserID @UserID";
            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    command.Parameters.Add("@UserID", SqlDbType.Int).Value = UserID;
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dt);
                    }
                }
            }
            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }

   

        [HttpGet]
        [Route("UpdateOrderDetailsByOrderID/{NewProductCode}/{ReferenceNo}/{ProxyId}")]
        public IActionResult UpdateOrderDetailsByOrderID(string NewProductCode, string ReferenceNo, int ProxyId)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            var command = new SqlCommand("spUpdateOrderDetailsByOrderID", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@NewProductCode", NewProductCode);
            command.Parameters.AddWithValue("@ReferenceNo", ReferenceNo);
            command.Parameters.AddWithValue("@ProxyId", ProxyId);


            SqlDataReader da = command.ExecuteReader();
            dt.Load(da);
            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("UpdateAnticipatedShipDate/{OrderId}/{AnticipatedShipDate}")]
        public IActionResult UpdateAnticipatedShipDate(string OrderId, string AnticipatedShipDate)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            var command = new SqlCommand("spUpdateAnticipatedShipDate", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderId", OrderId);
            command.Parameters.AddWithValue("@AnticipatedShipDate", AnticipatedShipDate);
            SqlDataReader da = command.ExecuteReader();
            dt.Load(da);
            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }

        [HttpGet]
        [Route("CompleteOrder/{OrderId}/{ActualShipDate}")]
        public IActionResult CompleteOrder(string OrderId, string ActualShipDate)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");

            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            try
            {

                SqlConnection connection = new SqlConnection(connectionString);
                connection.Open();

                var command = new SqlCommand("spCompleteOrder", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@OrderId", OrderId);
                command.Parameters.AddWithValue("@ActualShipDate", ActualShipDate);
                SqlDataReader da = command.ExecuteReader();
                dt.Load(da);
            }
            catch (Exception e)
            {
                message = e.InnerException;
            }
            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }

        [Route("updateUserInfoByUserID")]
        [HttpPost]
        public List<Users> updateUserInfoByUserID(Users User)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "updateUserInfoByUserID";
            var result = new List<Users>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Userid", User.UserID);
                //command.Parameters.AddWithValue("@Siteid", User.Siteid);
                //command.Parameters.AddWithValue("@EmailAddress", User.EmailAddress);
                command.Parameters.AddWithValue("@FirstName", User.FirstName);
                command.Parameters.AddWithValue("@LastName", User.LastName);
                //command.Parameters.AddWithValue("@SGID", User.SGID);
                command.Parameters.AddWithValue("@CompanyName", User.CompanyName);
                command.Parameters.AddWithValue("@Address", User.Address);
                command.Parameters.AddWithValue("@City", User.City);
                command.Parameters.AddWithValue("@State", User.State);
                command.Parameters.AddWithValue("@ZipCode", User.ZipCode);
                command.Parameters.AddWithValue("@Country", User.Country);
                command.Parameters.AddWithValue("@Phone", User.Phone);
                //command.Parameters.AddWithValue("@SecurityLevel", User.SecurityLevel);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        // User.Siteid = int.Parse(reader["Siteid"].ToString());
                        // User.EmailAddress = reader["EmailAddress"].ToString();
                        User.FirstName = reader["FirstName"].ToString();
                        User.LastName = reader["LastName"].ToString();
                        // User.SGID = reader["SGID"].ToString();
                        User.CompanyName = reader["CompanyName"].ToString();
                        User.Address = reader["Address"].ToString();
                        User.City = reader["City"].ToString();
                        User.State = reader["State"].ToString();
                        User.ZipCode = reader["ZipCode"].ToString();
                        User.Country = reader["Country"].ToString();
                        User.Phone = reader["Phone"].ToString();
                        // User.SecurityLevel = reader["SecurityLevel"].ToString();
                        Users tmpRecord = new Users()
                        {
                            //   Siteid = User.Siteid,
                            //   EmailAddress = User.EmailAddress,
                            FirstName = User.FirstName,
                            LastName = User.LastName,
                            //   SGID = User.SGID,
                            CompanyName = User.CompanyName,
                            Address = User.Address,
                            City = User.City,
                            State = User.State,
                            ZipCode = User.ZipCode,
                            Country = User.Country,
                            Phone = User.Phone,
                            //   SecurityLevel = User.SecurityLevel,
                        };
                        result.Add(tmpRecord);
                    }
                }
                con.Close();
            }
            return result;
        }
        [HttpGet]
        [Route("GetOrdersInfoByOrderNumber/{CTOrderNumber}")]
        public IActionResult GetOrdersInfoByOrderNumber(string CTOrderNumber)
        {
            string connectionString = _configuration.GetConnectionString("Dev_CTCustomization");
            string queryString = "EXEC spGetOrdersInfoByOrderNumber @CTOrderNumber";
            DataTable dt = new DataTable();
            string jsonString = string.Empty;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                using (SqlCommand command = new SqlCommand(queryString, connection))
                {
                    command.Parameters.Add("@CTOrderNumber", SqlDbType.VarChar).Value = CTOrderNumber;
                    connection.Open();
                    using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                    {
                        adapter.Fill(dt);
                    }
                }
            }
            if (dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                return Ok(jsonString);
            }
            return NotFound();
        }


        [Route("CreateOrder")]
        [HttpPost]
        public List<tblOrders> CreateOrders(tblOrders Order)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spCreateOrder";
            var result = new List<tblOrders>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Siteid", Order.Siteid);
                command.Parameters.AddWithValue("@Userid", Order.Userid);
                command.Parameters.AddWithValue("@CTOrderNumber", Order.CTOrderNumber);
                command.Parameters.AddWithValue("@CTPONumber", Order.CTPONumber);
                command.Parameters.AddWithValue("@StatusId", Order.StatusId);
                command.Parameters.AddWithValue("@EnteredBy", Order.EnteredBy);
                //command.Parameters.AddWithValue("@DateEntered", Order.DateEntered);
                command.Parameters.AddWithValue("@OrderType", Order.OrderType);

                command.Parameters.AddWithValue("@CTShipToInformation_AccountNumber", Order.CTShipToInformation_AccountNumber);
                command.Parameters.AddWithValue("@CTShipToInformation_CompanyName", Order.CTShipToInformation_CompanyName);
                command.Parameters.AddWithValue("@CTShipToInformation_Name", Order.CTShipToInformation_Name);
                command.Parameters.AddWithValue("@CTShipToInformation_Address", Order.CTShipToInformation_Address);
                command.Parameters.AddWithValue("@CTShipToInformation_City", Order.CTShipToInformation_City);
                command.Parameters.AddWithValue("@CTShipToInformation_State", Order.CTShipToInformation_State);
                command.Parameters.AddWithValue("@CTShipToInformation_ZipCode", Order.CTShipToInformation_ZipCode);
                command.Parameters.AddWithValue("@CTShipToInformation_Country", Order.CTShipToInformation_Country);
                command.Parameters.AddWithValue("@CTShipToInformation_Phone", Order.CTShipToInformation_Phone);
                command.Parameters.AddWithValue("@CTShipToInformation_EmailAddress", Order.CTShipToInformation_EmailAddress);
                command.Parameters.AddWithValue("@RepeatOrderComment", Order.RepeatOrderComment);
                command.Parameters.AddWithValue("@ArtworkID", Order.ArtworkID);
                command.Parameters.AddWithValue("@ProofID", Order.ProofID);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        /*Order.Siteid = int.Parse(reader["Siteid"].ToString());
                        Order.Userid = int.Parse(reader["Userid"].ToString());
                        Order.CTOrderNumber = reader["CTOrderNumber"].ToString();
                        Order.CTPONumber = reader["CTPONumber"].ToString();
                        Order.AnticipatedShipDate = DateTime.Parse(reader["AnticipatedShipDate"].ToString());
                        Order.StatusId = int.Parse(reader["StatusId"].ToString());
                        Order.EnteredBy = reader["EnteredBy"].ToString();
                        Order.DateEntered = DateTime.Parse(reader["DateEntered"].ToString());
                        Order.DateUpdated = DateTime.Parse(reader["DateUpdated"].ToString());
                        Order.OrderType = reader["OrderType"].ToString();
                        Order.CTShipToInformation_AccountNumber = reader["CTShipToInformation_AccountNumber"].ToString();
                        Order.CTShipToInformation_CompanyName = reader["CTShipToInformation_CompanyName"].ToString();
                        Order.CTShipToInformation_Name = reader["CTShipToInformation_Name"].ToString();
                        Order.CTShipToInformation_Address = reader["CTShipToInformation_Address"].ToString();
                        Order.CTShipToInformation_City = reader["CTShipToInformation_City"].ToString();
                        Order.CTShipToInformation_State = reader["CTShipToInformation_State"].ToString();
                        Order.CTShipToInformation_ZipCode = reader["CTShipToInformation_ZipCode"].ToString();
                        Order.CTShipToInformation_Country = reader["CTShipToInformation_Country"].ToString();
                        Order.CTShipToInformation_Phone = reader["CTShipToInformation_Phone"].ToString();
                        Order.CTShipToInformation_EmailAddress = reader["CTShipToInformation_EmailAddress"].ToString();
                        Order.TerritoryNumber = reader["TerritoryNumber"].ToString();
                        Order.RegionNumber = reader["RegionNumber"].ToString();
                        Order.ArtworkID = reader["ArtworkID"].ToString();
                        Order.ProofID = reader["ProofID"].ToString();
                        Order.RepeatOrderComment = reader["RepeatOrderComment"].ToString();*/
                        Order.OrderID = reader["OrderID"].ToString();
                        Order.CTOrderNumber = reader["CTOrderNumber"].ToString();

                        tblOrders tmpRecord = new tblOrders()
                        {
                            OrderID = Order.OrderID,
                            CTOrderNumber = Order.CTOrderNumber
                            // Siteid = Order.Siteid,
                            //Userid = Order.Userid,
                            /*,CTPONumber  = Order.CTPONumber ,
                            AnticipatedShipDate = Order.AnticipatedShipDate,
                            StatusId  = Order.StatusId ,
                            EnteredBy  = Order.EnteredBy ,
                            DateEntered  = Order.DateEntered ,
                            DateUpdated  = Order.DateUpdated ,
                            OrderType = Order.OrderType,

                            CTShipToInformation_AccountNumber  = Order.CTShipToInformation_AccountNumber ,
                            CTShipToInformation_CompanyName  = Order.CTShipToInformation_CompanyName ,
                            CTShipToInformation_Name = Order.CTShipToInformation_Name,
                            CTShipToInformation_Address  = Order.CTShipToInformation_Address ,
                            CTShipToInformation_City  = Order.CTShipToInformation_City ,
                            CTShipToInformation_State = Order.CTShipToInformation_State,
                            CTShipToInformation_ZipCode  = Order.CTShipToInformation_ZipCode ,
                            CTShipToInformation_Country = Order.CTShipToInformation_Country,
                            CTShipToInformation_Phone = Order.CTShipToInformation_Phone,
                            CTShipToInformation_EmailAddress  = Order.CTShipToInformation_EmailAddress ,
                            TerritoryNumber  = Order.TerritoryNumber ,
                            RegionNumber = Order.RegionNumber,
                            ArtworkID  = Order.ArtworkID ,
                            ProofID = Order.ProofID,
                            RepeatOrderComment = Order.RepeatOrderComment,*/

                        };
                        // SendEmail("",tmpRecord.OrderID,"localhost");
                        result.Add(tmpRecord);

                        // }
                    }
                }
                con.Close();
            }
            return result;
        }
        
        [Route("UpdateOrder")]
        [HttpPost]
        public List<tblOrders> UpdateOrder(tblOrders Order)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spEditOrder";
            var result = new List<tblOrders>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@OrderID", Order.OrderID);
                command.Parameters.AddWithValue("@Siteid", Order.Siteid);
                command.Parameters.AddWithValue("@Userid", Order.Userid);
                command.Parameters.AddWithValue("@CTOrderNumber", Order.CTOrderNumber);
                command.Parameters.AddWithValue("@CTPONumber", Order.CTPONumber);
                command.Parameters.AddWithValue("@StatusId", Order.StatusId);
                command.Parameters.AddWithValue("@EnteredBy", Order.EnteredBy);
                command.Parameters.AddWithValue("@OrderType", Order.OrderType);
                command.Parameters.AddWithValue("@CTShipToInformation_AccountNumber", Order.CTShipToInformation_AccountNumber);
                command.Parameters.AddWithValue("@CTShipToInformation_CompanyName", Order.CTShipToInformation_CompanyName);
                command.Parameters.AddWithValue("@CTShipToInformation_Name", Order.CTShipToInformation_Name);
                command.Parameters.AddWithValue("@CTShipToInformation_City", Order.CTShipToInformation_City);
                command.Parameters.AddWithValue("@CTShipToInformation_State", Order.CTShipToInformation_State);
                command.Parameters.AddWithValue("@CTShipToInformation_ZipCode", Order.CTShipToInformation_ZipCode);
                command.Parameters.AddWithValue("@CTShipToInformation_Country", Order.CTShipToInformation_Country);
                command.Parameters.AddWithValue("@CTShipToInformation_Phone", Order.CTShipToInformation_Phone);
                command.Parameters.AddWithValue("@CTShipToInformation_EmailAddress", Order.CTShipToInformation_EmailAddress);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        Order.OrderID = (reader["OrderID"].ToString());
                        //Order.OrderID = Guid.Parse(reader["OrderID"].ToString());
                        Order.Siteid = int.Parse(reader["Siteid"].ToString());
                        Order.Userid = int.Parse(reader["Userid"].ToString());
                        Order.CTOrderNumber = reader["CTOrderNumber"].ToString();
                        Order.CTPONumber = reader["CTPONumber"].ToString();
                        Order.StatusId = int.Parse(reader["StatusId"].ToString());
                        Order.EnteredBy = reader["EnteredBy"].ToString();
                        Order.OrderType = reader["OrderType"].ToString();
                        Order.CTShipToInformation_AccountNumber = reader["CTShipToInformation_AccountNumber"].ToString();
                        Order.CTShipToInformation_CompanyName = reader["CTShipToInformation_CompanyName"].ToString();
                        Order.CTShipToInformation_Name = reader["CTShipToInformation_Name"].ToString();
                        Order.CTShipToInformation_City = reader["CTShipToInformation_City"].ToString();
                        Order.CTShipToInformation_State = reader["CTShipToInformation_State"].ToString();
                        Order.CTShipToInformation_ZipCode = reader["CTShipToInformation_ZipCode"].ToString();
                        Order.CTShipToInformation_Country = reader["CTShipToInformation_Country"].ToString();
                        Order.CTShipToInformation_Phone = reader["CTShipToInformation_Phone"].ToString();
                        Order.CTShipToInformation_EmailAddress = reader["CTShipToInformation_EmailAddress"].ToString();
                        tblOrders tmpRecord = new tblOrders()
                        {
                            //OrderID = Order.OrderID,
                            Siteid = Order.Siteid,
                            Userid = Order.Userid,
                            CTOrderNumber = Order.CTOrderNumber,
                            CTPONumber = Order.CTPONumber,
                            StatusId = Order.StatusId,
                            EnteredBy = Order.EnteredBy,
                            OrderType = Order.OrderType,
                            CTShipToInformation_AccountNumber = Order.CTShipToInformation_AccountNumber,
                            CTShipToInformation_CompanyName = Order.CTShipToInformation_CompanyName,
                            CTShipToInformation_Name = Order.CTShipToInformation_Name,
                            CTShipToInformation_City = Order.CTShipToInformation_City,
                            CTShipToInformation_State = Order.CTShipToInformation_State,
                            CTShipToInformation_ZipCode = Order.CTShipToInformation_ZipCode,
                            CTShipToInformation_Country = Order.CTShipToInformation_Country,
                            CTShipToInformation_Phone = Order.CTShipToInformation_Phone,
                            CTShipToInformation_EmailAddress = Order.CTShipToInformation_EmailAddress,
                        };
                        result.Add(tmpRecord);
                    }
                }
                con.Close();
            }
            return result;
        }
        
        // delete
        [HttpDelete]
        [Route("DeleteOrder/{OrderID}")]
        public string DeleteOrder(string OrderID)
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            conn.Open();
            var command = new SqlCommand("spDeleteOrder", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderID", OrderID);
            SqlDataReader da = command.ExecuteReader();
            string jsonString = string.Empty;
            DataTable dt = new DataTable();
            dt.Load(da);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            conn.Close();
            return jsonString;
        }

        [Route("CheckUser")]
        [HttpPost]
        public List<Users> CheckUser(Users User)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spCheckUserEmailAddress";
            var result = new List<Users>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
               
                command.Parameters.AddWithValue("@EmailAddress", User.EmailAddress);
              

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                       
                        User.UserID = int.Parse(reader["UserID"].ToString());
                       

                        Users tmpRecord = new Users()
                        {
                           
                            UserID = User.UserID,

                        };
                        result.Add(tmpRecord);
                    }
                }
                con.Close();
            }
            return result;
        }

        [Route("CreateUser")]
        [HttpPost]
        public List<Users> CreateUser(Users User)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spCreateUser";
            var result = new List<Users>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Siteid", User.Siteid);
                command.Parameters.AddWithValue("@EmailAddress", User.EmailAddress);
                command.Parameters.AddWithValue("@FirstName", User.FirstName);
                command.Parameters.AddWithValue("@LastName", User.LastName);
                command.Parameters.AddWithValue("@SGID", User.SGID);
                command.Parameters.AddWithValue("@CompanyName", User.CompanyName);
                command.Parameters.AddWithValue("@Address", User.Address);
                command.Parameters.AddWithValue("@City", User.City);
                command.Parameters.AddWithValue("@State", User.State);
                command.Parameters.AddWithValue("@ZipCode", User.ZipCode);
                command.Parameters.AddWithValue("@Country", User.Country);
                command.Parameters.AddWithValue("@Phone", User.Phone);
                command.Parameters.AddWithValue("@SecurityLevel", User.SecurityLevel);
                command.Parameters.AddWithValue("@OktaUserId", User.oktauserid);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        User.Siteid = int.Parse(reader["Siteid"].ToString());
                        User.EmailAddress = reader["EmailAddress"].ToString();
                        User.FirstName = reader["FirstName"].ToString();
                        User.LastName = reader["LastName"].ToString();
                        User.SGID = reader["SGID"].ToString();
                        User.CompanyName = reader["CompanyName"].ToString();
                        User.Address = reader["Address"].ToString();
                        User.City = reader["City"].ToString();
                        User.State = reader["State"].ToString();
                        User.ZipCode = reader["ZipCode"].ToString();
                        User.Country = reader["Country"].ToString();
                        User.Phone = reader["Phone"].ToString();
                        User.SecurityLevel = reader["SecurityLevel"].ToString();

                        Users tmpRecord = new Users()
                        {
                            Siteid = User.Siteid,
                            EmailAddress = User.EmailAddress,
                            FirstName = User.FirstName,
                            LastName = User.LastName,
                            SGID = User.SGID,
                            CompanyName = User.CompanyName,
                            Address = User.Address,
                            City = User.City,
                            State = User.State,
                            ZipCode = User.ZipCode,
                            Country = User.Country,
                            Phone = User.Phone,
                            SecurityLevel = User.SecurityLevel,

                        };
                        result.Add(tmpRecord);
                    }
                }
                con.Close();
            }
            return result;
        }

        
        [Route("UpdateUser")]
        [HttpPost]
        public List<Users> UpdateUser(Users User)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spUpdateUser";
            var result = new List<Users>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@Userid", User.UserID);
                command.Parameters.AddWithValue("@Siteid", User.Siteid);
                command.Parameters.AddWithValue("@EmailAddress", User.EmailAddress);
                command.Parameters.AddWithValue("@FirstName", User.FirstName);
                command.Parameters.AddWithValue("@LastName", User.LastName);
                command.Parameters.AddWithValue("@SGID", User.SGID);
                command.Parameters.AddWithValue("@CompanyName", User.CompanyName);
                command.Parameters.AddWithValue("@Address", User.Address);
                command.Parameters.AddWithValue("@City", User.City);
                command.Parameters.AddWithValue("@State", User.State);
                command.Parameters.AddWithValue("@ZipCode", User.ZipCode);
                command.Parameters.AddWithValue("@Country", User.Country);
                command.Parameters.AddWithValue("@Phone", User.Phone);
                command.Parameters.AddWithValue("@SecurityLevel", User.SecurityLevel);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        User.Siteid = int.Parse(reader["Siteid"].ToString());
                        User.EmailAddress = reader["EmailAddress"].ToString();
                        User.FirstName = reader["FirstName"].ToString();
                        User.LastName = reader["LastName"].ToString();
                        User.SGID = reader["SGID"].ToString();
                        User.CompanyName = reader["CompanyName"].ToString();
                        User.Address = reader["Address"].ToString();
                        User.City = reader["City"].ToString();
                        User.State = reader["State"].ToString();
                        User.ZipCode = reader["ZipCode"].ToString();
                        User.Country = reader["Country"].ToString();
                        User.Phone = reader["Phone"].ToString();
                        User.SecurityLevel = reader["SecurityLevel"].ToString();

                        Users tmpRecord = new Users()
                        {
                            Siteid = User.Siteid,
                            EmailAddress = User.EmailAddress,
                            FirstName = User.FirstName,
                            LastName = User.LastName,
                            SGID = User.SGID,
                            CompanyName = User.CompanyName,
                            Address = User.Address,
                            City = User.City,
                            State = User.State,
                            ZipCode = User.ZipCode,
                            Country = User.Country,
                            Phone = User.Phone,
                            SecurityLevel = User.SecurityLevel,

                        };
                        result.Add(tmpRecord);
                    }
                }
                con.Close();
            }
            return result;
        }

        [Route("InserProductDetails")]
        [HttpPost]
        public List<tblOrderDetails> InserProductDetails(tblOrderDetails Order)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spInsertProductDetails";
            var result = new List<tblOrderDetails>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@OrderID", Order.OrderID);
                command.Parameters.AddWithValue("@ProductCode", Order.ProductCode);
                command.Parameters.AddWithValue("@ProductDescription", Order.ProductDescription);
                command.Parameters.AddWithValue("@ProductQty", Order.ProductQty);
                command.Parameters.AddWithValue("@ReferenceNo", Order.ReferenceNo);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        Order.OrderID = reader["Siteid"].ToString();
                        Order.ProductCode = reader["Userid"].ToString();
                        Order.ProductDescription = reader["CTOrderNumber"].ToString();
                        Order.ProductQty = reader["CTPONumber"].ToString();
                        Order.ReferenceNo = reader["ReferenceNo"].ToString();


                        tblOrderDetails tmpRecord = new tblOrderDetails()
                        {
                            OrderID = Order.OrderID,
                            ProductCode = Order.ProductCode,
                            ProductDescription = Order.ProductDescription,
                            ProductQty = Order.ProductQty,
                            ReferenceNo = Order.ReferenceNo


                        };
                        result.Add(tmpRecord);
                        // }
                    }
                }
                con.Close();
            }
            return result;
        }
        [Route("UpdateProductDetails")]
        [HttpPost]
        public List<tblOrderDetails> UpdateProductDetails(List<tblOrderDetails> orders)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spEditProductDetails";
            var result = new List<tblOrderDetails>();

            foreach (var order in orders)
            {
                using (SqlCommand command = new SqlCommand(procedureName, con))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    command.Parameters.AddWithValue("@OrderID", order.OrderID);
                    command.Parameters.AddWithValue("@ProxyID", order.ProxyID);
                    command.Parameters.AddWithValue("@ProductCode", order.ProductCode);
                    command.Parameters.AddWithValue("@ProductDescription", order.ProductDescription);
                    command.Parameters.AddWithValue("@ProductQty", order.ProductQty);
                    command.Parameters.AddWithValue("@ReferenceNo", order.ReferenceNo);

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            tblOrderDetails tmpRecord = new tblOrderDetails()
                            {
                                ProxyID = int.Parse(reader["ProxyID"].ToString()),
                                OrderID = reader["OrderID"].ToString(),
                                ProductCode = reader["ProductCode"].ToString(),
                                ProductDescription = reader["ProductDescription"].ToString(),
                                ProductQty = reader["ProductQty"].ToString(),
                                ReferenceNo = reader["ReferenceNo"].ToString()
                            };
                            result.Add(tmpRecord);
                        }
                    }
                }
            }

            con.Close();

            return result;
        }

        [Route("InsertUpdateCustomerEmails")]
        [HttpPost]
        public List<tblCustomerEmails> InsertUpdateCustomerEmails(tblCustomerEmails Customer)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spInsertUpdateCustomerEmails";
            var result = new List<tblCustomerEmails>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@OrderID", Customer.OrderID);
                command.Parameters.AddWithValue("@AssignedUserid", Customer.Userid);
                command.Parameters.AddWithValue("@OrderByNo", Customer.OrderByAccountNumber);
                command.Parameters.AddWithValue("@ShipToNo", Customer.ShipToAccountNumber);
                command.Parameters.AddWithValue("@FullName", Customer.FullName);
                command.Parameters.AddWithValue("@CustEmail", Customer.EmailAddress);
                command.Parameters.AddWithValue("@Userid", Customer.Userid);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        Customer.OrderID = reader["OrderID"].ToString();
                        Customer.Userid = int.Parse(reader["Userid"].ToString());
                        Customer.OrderByAccountNumber = reader["OrderByAccountNumber"].ToString();
                        Customer.ShipToAccountNumber = reader["ShipToAccountNumber"].ToString();
                        Customer.EnteredBy = reader["EnteredBy"].ToString();

                        tblCustomerEmails tmpRecord = new tblCustomerEmails()
                        {
                            OrderID = Customer.OrderID,
                            Userid = Customer.Userid,
                            OrderByAccountNumber = Customer.OrderByAccountNumber,
                            ShipToAccountNumber = Customer.ShipToAccountNumber,
                            EnteredBy = Customer.EnteredBy


                        };
                        result.Add(tmpRecord);
                        // }
                    }
                }
                con.Close();
            }
            return result;
        }


        [Route("InsertArtwork")]
        [HttpPost]
        public List<tblArtwork> spInsertArtwork(tblArtwork artwork)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spInsertArtwork";
            var result = new List<tblArtwork>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@UserId", artwork.UserId);
                command.Parameters.AddWithValue("@FileName", artwork.FileName);
                command.Parameters.AddWithValue("@OrigFileName", artwork.OrigFileName);
                command.Parameters.AddWithValue("@DateAdded", artwork.DateAdded);
                command.Parameters.AddWithValue("@PMSColors", artwork.PMSColors);
                command.Parameters.AddWithValue("@OrderId", artwork.OrderId);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        artwork.ArtworkID = reader["ArtworkID"].ToString();
                        //var EmailAddress = reader["ArtworkID"].ToString();
                        tblArtwork tmpRecord = new tblArtwork()
                        {
                            UserId = artwork.UserId,
                            FileName = artwork.FileName,
                            OrigFileName = artwork.OrigFileName,
                            DateAdded = artwork.DateAdded,
                            PMSColors = artwork.PMSColors,
                            OrderId = artwork.OrderId,
                            ArtworkID = artwork.ArtworkID
                        };

                        result.Add(tmpRecord);
                    }
                }

                con.Close();
            }

            return result;
        }

        /*[Route("InsertArtworkFile")]
        [HttpPost]
        public async Task<IActionResult> InsertArtworkFileAsync(IFormFile uploadNewFile, [FromForm] string orderId, [FromForm] string userId, [FromForm] string pmsColors)
        {
          //  var rootPath = Environment.GetFolderPath("D:\\Websites\\productcustomization.certainteed.com\\Productcustomization\\assets");
          
          //  var fullPath =Path.Combine("D:\\Websites\\productcustomization.certainteed.com\\Productcustomization\\assets\\Files"); ;
           // var a = Directory.Exists(fullPath);
            //try
            //{
            //    if (uploadNewFile == null || uploadNewFile.Length == 0)
            //        return BadRequest("File not selected");

                var fileName = orderId+"_"+uploadNewFile.FileName;

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "files", fileName);
            try
            {
                using (var stream = new MemoryStream())
                {
                    uploadNewFile.CopyTo(stream);
                    
                }
                using (FileStream stream = new FileStream(filePath, FileMode.Create))
                {
                    await uploadNewFile.CopyToAsync(stream);
                    stream.Close();
                }

                return Ok(filePath);
            }

            catch (Exception ex)
            {
                return Ok(ex.InnerException);
            }
            
        }*/

            [Route("InsertArtworkFile")]
            [HttpPost]
            public async Task<IActionResult> InsertArtworkFileAsync(IFormFile uploadNewFile, [FromForm] string orderId, [FromForm] string userId, [FromForm] string pmsColors)
            {
                //  var rootPath = Environment.GetFolderPath("D:\\Websites\\productcustomization.certainteed.com\\Productcustomization\\assets");
                //  var fullPath =Path.Combine("D:\\Websites\\productcustomization.certainteed.com\\Productcustomization\\assets\\Files"); ;
                // var a = Directory.Exists(fullPath);
                //try
                //{
                //    if (uploadNewFile == null || uploadNewFile.Length == 0)
                //        return BadRequest("File not selected");

                var fileName = orderId + "_" + uploadNewFile.FileName;

                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "files", fileName);
                try
                {
                    using (var stream = new MemoryStream())
                    {
                        uploadNewFile.CopyTo(stream);
                    }
                    using (FileStream stream = new FileStream(filePath, FileMode.Create))
                    {
                        await uploadNewFile.CopyToAsync(stream);
                        stream.Close();
                    }

                    return Ok(filePath);
                }

                catch (Exception ex)
                {
                    return Ok(ex.InnerException);
                }
                //}

                //catch(Exception e)
                //{
                //    return Ok(e.InnerException);
                //}


            }

            [Route("DownloadArtworkFile/{orderId}/{fileName}")]
            [HttpGet]
            public IActionResult DownloadArtworkFile(string orderId, string fileName)
            {
                var file = $"{orderId}_{fileName}";
            
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "files", file);
                if (System.IO.File.Exists(filePath))
                {
                    // Set response headers
                    Response.Headers["Content-Type"] = "application/octet-stream";
                    Response.Headers["Content-Disposition"] = $"attachment; filename={file}";

                    var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                    return File(fileStream, "application/octet-stream");
                }
                else
                {
                    return NotFound();
                }
            }

            [Route("GetArtworkToDownload")]
            [HttpGet]
            public string GetArtworkToDownload(string OrderID)
            {
                SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
                conn.Open();
                var command = new SqlCommand("spGetArtworkToDownload", conn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@OrderID", OrderID);
                SqlDataReader da = command.ExecuteReader();
                string jsonString = string.Empty;
                DataTable dt = new DataTable();
                dt.Load(da);
                if (dt != null && dt.Rows.Count > 0)
                {
                    jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
                }
                conn.Close();
                return jsonString;
            }


        [Route("GetPMSColor/{OrderID}")]
        [HttpGet]
        public string GetPMSColor(Guid OrderID) // Change parameter type to Guid
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            conn.Open();
            var command = new SqlCommand("spGetPMSColor", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderID", OrderID); // Pass OrderID directly
            SqlDataReader da = command.ExecuteReader();
            string jsonString = string.Empty;
            DataTable dt = new DataTable();
            dt.Load(da);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            conn.Close();
            return jsonString;
        }

        [Route("InsertProof")]
        [HttpPost]
        public List<tblProofs> spInsertProof(tblProofs proof)
        {
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spInsertProof";
            var result = new List<tblProofs>();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@UserId", proof.UserId);
                command.Parameters.AddWithValue("@FileName", proof.FileName);
                command.Parameters.AddWithValue("@OrigFileName", proof.OrigFileName);
                command.Parameters.AddWithValue("@DateAdded", proof.DateAdded);
                command.Parameters.AddWithValue("@CustomerComments", proof.CustomerComments);
                command.Parameters.AddWithValue("@CustomerStatus", proof.CustomerStatus);
                command.Parameters.AddWithValue("@PMSColors", proof.PMSColors);
                command.Parameters.AddWithValue("@OrderId", proof.OrderId);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        tblProofs tmpRecord = new tblProofs()
                        {
                            UserId = proof.UserId,
                            FileName = proof.FileName,
                            OrigFileName = proof.OrigFileName,
                            DateAdded = proof.DateAdded,
                            PMSColors = proof.PMSColors,
                            OrderId = proof.OrderId
                        };

                        result.Add(tmpRecord);
                    }
                }

                con.Close();
            }

            return result;
        }

        /*[Route("InsertProofFile")]
        [HttpPost]
        public IActionResult InsertProofFile(IFormFile uploadNewFile, [FromForm] string orderId, [FromForm] string userId)
        {
            if (uploadNewFile == null || uploadNewFile.Length == 0)
                return BadRequest("File not selected");

            var fileName = orderId + "_" + uploadNewFile.FileName;
            var filePath = Path.Combine(_environment.ContentRootPath, "Files", "Proof", fileName);

            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                uploadNewFile.CopyTo(stream);
            }

            return Ok(new { fileName });
        }*/

        [Route("InsertProofFile")]
        [HttpPost]
        public async Task<IActionResult> InsertProofFileAsync(IFormFile uploadNewFile, [FromForm] string orderId, [FromForm] string userId)
        {
            var fileName = orderId + "_" + uploadNewFile.FileName;
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "files", fileName);
            
            try
            {
                using (var stream = new MemoryStream())
                {
                    uploadNewFile.CopyTo(stream);
                }
                using (FileStream stream = new FileStream(filePath, FileMode.Create))
                {
                    await uploadNewFile.CopyToAsync(stream);
                    stream.Close();
                }

                return Ok(filePath);
            }

            catch (Exception ex)
            {
                return Ok(ex.InnerException);
            }

        }

        [Route("DownloadProofFile/{orderId}/{fileName}")]
        [HttpGet]
        public IActionResult DownloadProofFile(string orderId, string fileName)
        {
            var file = $"{orderId}_{fileName}";
            var filePath = Path.Combine(_proofsFolderPath, file);

            if (System.IO.File.Exists(filePath))
            {
                // Set response headers
                Response.Headers["Content-Type"] = "application/octet-stream";
                Response.Headers["Content-Disposition"] = $"attachment; filename={file}";

                var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read);
                return File(fileStream, "application/octet-stream");
            }
            else
            {
                return NotFound();
            }
        }

        [Route("GetProofToDownload")]
        [HttpGet]
        public string GetProofToDownload(string OrderID)
        {
            SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            conn.Open();
            var command = new SqlCommand("spGetProofToDownload", conn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@OrderID", OrderID);
            SqlDataReader da = command.ExecuteReader();
            string jsonString = string.Empty;
            DataTable dt = new DataTable();
            dt.Load(da);
            if (dt != null && dt.Rows.Count > 0)
            {
                jsonString = JsonConvert.SerializeObject(dt, Formatting.Indented);
            }
            conn.Close();
            return jsonString;
        }

        [HttpPost]
        [Route("UpdateOrderStatus")]
        public IActionResult UpdateOrderStatus([FromBody] OrderStatusUpdateRequest request)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization")))
                {
                    using (SqlCommand command = new SqlCommand("spUpdateOrderStatus", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@OrderID", request.OrderID);
                        command.Parameters.AddWithValue("@StatusID", request.StatusID);

                        connection.Open();
                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            // Fetch updated status data
                            var updatedStatus = new
                            {
                                OrderID = request.OrderID,
                                StatusID = request.StatusID,
                                // Add more properties as needed
                            };

                            // Return the updated status data
                            return Ok(updatedStatus);
                        }
                        else
                        {
                            return NotFound("Order not found or status not updated.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }
        
        [HttpPost]
        [Route("InsertLog")]
        public IActionResult InsertLog([FromBody] LogRequest request)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization")))
                {
                    using (SqlCommand command = new SqlCommand("spInsertLog", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@UserID", request.UserID);
                        command.Parameters.AddWithValue("@IPAddress", request.IPAddress);
                        command.Parameters.AddWithValue("@OrderID", request.OrderID);
                        command.Parameters.AddWithValue("@ActionType", request.ActionType);
                        command.Parameters.AddWithValue("@ActionDescription", request.ActionDescription);
                        command.Parameters.AddWithValue("@BrowserInfo", request.BrowserInfo);
                        command.Parameters.AddWithValue("@Comments", request.Comments);
                        command.Parameters.AddWithValue("@Status", request.Status);
                        connection.Open();
                        int rowsAffected = command.ExecuteNonQuery();
                        return Ok(rowsAffected);
                    }
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPost]
        [Route("SendEmail/{Template}/{Servername}/{ExtraUrl}/{FullName}/{OrderNo}/{Site}/{ToEmail}")]
        public async Task<IActionResult> SendEmail(string Template, string Servername, string ExtraUrl, string FullName, string OrderNo, string Site, string ToEmail)
        {
            Servername = Servername.Replace("%2F", "/");
            if (ExtraUrl == "null")
            {
                ExtraUrl = "";
            }
            string body = this.PopulateBody(Template, Servername, ExtraUrl, FullName, OrderNo, Site);
            MailRequest request = new MailRequest();
            request.Subject = "Upload Artwork for Order #" + OrderNo + " -- CertainTeed Roofing Customized " + Site;
            request.Body = body;
            request.ToEmail = ToEmail;

            //MailController mailController = new MailController(mailService);
            //await mailController.SendMail(request);
            SendEmailAsync(ToEmail, body, "Upload Artwork for Order #" + OrderNo + " -- CertainTeed Roofing Customized " + Site);
            return Ok();
        }

        [HttpPost]
        [Route("RepeatOrder/{Template}/{Servername}/{ExtraUrl}/{FullName}/{OrderNo}/{Site}/{ToEmail}")]
        public async Task<IActionResult> RepeatOrder(string Template, string Servername, string ExtraUrl, string FullName, string OrderNo, string Site, string ToEmail)
        {
            Servername = Servername.Replace("%2F", "/");
            if (ExtraUrl == "null")
            {
                ExtraUrl = "";
            }
            string body = this.PopulateBody(Template, Servername, ExtraUrl, FullName, OrderNo, Site);
            MailRequest request = new MailRequest();
            request.Subject = "Confirm Order #" + OrderNo + " -- CertainTeed Roofing Customized " + Site;
            request.Body = body;
            request.ToEmail = ToEmail;

            //MailController mailController = new MailController(mailService);
            //await mailController.SendMail(request);
            SendEmailAsync(ToEmail, body, "Confirm Order #" + OrderNo + " -- CertainTeed Roofing Customized " + Site);
            return Ok();
        }
        private string PopulateBody(string Template, string Servername, string ExtraUrl, string FullName, string OrderNo, string Site)
        {
            string body = string.Empty;
            //string directoryPath = @"C:\Users\T3776844\OneDrive - Saint-Gobain\Documents\ct-customization-backend\CTSites\EmailTemplates";
            string directoryPath = @"D:\Websites\productcustomization.certainteed.com\API\EmailTemplates";
            string filePath = Path.Combine(directoryPath, Template + ".html");

            using (StreamReader reader = new StreamReader(filePath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("##SERVERNAME##", Servername);

            body = body.Replace("##EXTRAURL##", ExtraUrl);

            body = body.Replace("##DATE##", DateTime.Today.ToString());

            body = body.Replace("##DATETIME##", DateTime.Now.ToString());

            body = body.Replace("##FULLNAME##", FullName);

            body = body.Replace("##CTORDERNUMBER##", OrderNo);

            body = body.Replace("##Site##", Site);

            body = body.Replace("##SITEURL##", "<a href=\"http://" + Servername + ExtraUrl + "\">http://" + Servername + ExtraUrl + "</a>");

            body = body.Replace("##COPYRIGHT##", DateTime.Now.Year.ToString());
            return body;
        }

        [HttpPost]
        [Route("SendUploadArtworkEmail/{Template}/{Servername}/{ExtraUrl}/{OrderNo}/{Site}/{varExistingFileID}/{PMSCOLORS}/{ArtworkID}")]
        public async Task<IActionResult> SendUploadArtworkEmail(string Template, string Servername, string ExtraUrl, string OrderNo, string Site, string varExistingFileID, string PMSCOLORS, string ArtworkID)
        {
            Servername = Servername.Replace("%2F", "/");
            if (ExtraUrl == "null")
            {
                ExtraUrl = "";
            }
            string body = string.Empty;
            //string directoryPath = @"C:\Users\T3776844\OneDrive - Saint-Gobain\Documents\ct-customization-backend\CTSites\EmailTemplates";
            string directoryPath = @"D:\Websites\productcustomization.certainteed.com\API\EmailTemplates";
            string filePath = Path.Combine(directoryPath, Template + ".html");

            using (StreamReader reader = new StreamReader(filePath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("##SERVERNAME##", Servername);

            body = body.Replace("##EXTRAURL##", ExtraUrl);

            body = body.Replace("##DATE##", DateTime.Today.ToString());

            body = body.Replace("##DATETIME##", DateTime.Now.ToString());

            body = body.Replace("##CTORDERNUMBER##", OrderNo);

            body = body.Replace("##Site##", Site);

            body = body.Replace("##SITEURL##", "<a href=\"http://" + Servername + ExtraUrl + "\">http://" + Servername + ExtraUrl + "</a>");
            //body = body.Replace("##DOWNLOADURL##", "<a href=\"https://" + Servername + ExtraUrl + "/DownloadFile.asp?Vendor=True&Type=Artwork&ID=" + varExistingFileID + "\">http://" + Servername + ExtraUrl + "/DownloadFile.asp?Vendor=True&Type=Artwork&ID=" + varExistingFileID + "</a>");
            //body = body.Replace("##DOWNLOADURL##", "<a href=\"https://digital-team-805wb.certainteed.com/ProductCTAPI/api/Orders/" + ArtworkID + varExistingFileID + " </a>");
            body = body.Replace("##DOWNLOADURL##", "<a href=\"https://digital-team-805wb.certainteed.com/ProductCTAPI/api/Orders/DownloadArtworkFile/" + HttpUtility.UrlEncode(ArtworkID) + "/" + HttpUtility.UrlEncode(varExistingFileID) + "\">Download Artwork</a>");

            body = body.Replace("##PMSCOLORS##", PMSCOLORS);
            body = body.Replace("##COPYRIGHT##", DateTime.Now.Year.ToString());
            MailRequest request = new MailRequest();
            request.Subject = "Artwork Attached To Order #" + OrderNo + " -- CertainTeed Roofing Customized " + Site;
            request.Body = body;
            //request.ToEmail = "creativeservices@saint-gobain.com";
            request.ToEmail = "ashwini.mane@saint-gobain.com";
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spGetVendorEmailAddress";
            var result = new Users();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@SiteId", Site);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        result.EmailAddress = reader["EmailAddress"].ToString();
                        request.ToEmail = request.ToEmail + ", " + result.EmailAddress;


                    }
                }

                con.Close();

                SendEmailAsync(request.ToEmail, request.Body, request.Subject);

                /*MailController mailController = new MailController(mailService);
                await mailController.SendMail(request);*/
                return Ok();
            }

        }

        
        [HttpPost]
        [Route("SendUploadProofEmail/{Template}/{Servername}/{ExtraUrl}/{OrderNo}/{Site}/{varUserId}/{Fullname}")]
        public async Task<IActionResult> SendUploadProofEmail(string Template, string Servername, string ExtraUrl, string OrderNo, string Site, string varUserId, string Fullname)
        {
            Servername = Servername.Replace("%2F", "/");
            if (ExtraUrl == "null")
            {
                ExtraUrl = "";
            }
            string body = string.Empty;
           // string directoryPath = @"C:\CertainTeed Projects\Angular project\CT customize site\New Code\API Code\CTSites\EmailTemplates";
            string directoryPath = @"D:\Websites\productcustomization.certainteed.com\API\EmailTemplates";
            string filePath = Path.Combine(directoryPath, Template + ".html");

            using (StreamReader reader = new StreamReader(filePath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("##SERVERNAME##", Servername);

            body = body.Replace("##EXTRAURL##", ExtraUrl);

            body = body.Replace("##DATE##", DateTime.Today.ToString());

            body = body.Replace("##DATETIME##", DateTime.Now.ToString());

            body = body.Replace("##CTORDERNUMBER##", OrderNo);

            body = body.Replace("##Site##", Site);

            body = body.Replace("##SITEURL##", "<a href=\"http://" + Servername + ExtraUrl + "\">http://" + Servername + ExtraUrl + "</a>");
            body = body.Replace("##AUTOLOGINURL##", "https://" + Servername + ExtraUrl + "/Index.asp?Action=SubmitForm&UserID=" + varUserId + "&OrderID=" + OrderNo);
            body = body.Replace("##FULLNAME##", Fullname);
            body = body.Replace("##COPYRIGHT##", DateTime.Now.Year.ToString());
            MailRequest request = new MailRequest();
            request.Subject = "Proof Ready for Order #" + OrderNo + " -- CertainTeed Roofing Customized " + Site;
            request.Body = body;
            // request.ToEmail = "creativeservices@saint-gobain.com";
            request.ToEmail = "ashwini.mane@saint-gobain.com";
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spGetUserEmailAddress";
            var result = new Users();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@UserID", varUserId);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        result.EmailAddress = reader["EmailAddress"].ToString();
                        request.ToEmail = request.ToEmail + ", " + result.EmailAddress;


                    }
                }

                con.Close();

                SendEmailAsync(request.ToEmail, request.Body, request.Subject);

                /*MailController mailController = new MailController(mailService);
                await mailController.SendMail(request);*/
                return Ok();
            }

        }


        [HttpPost]
        [Route("SendArtworkRejectedByVendorEmail/{Template}/{Servername}/{ExtraUrl}/{OrderNo}/{Site}/{varUserId}/{Fullname}/{varExistingFileID}/{REJECTREASON}")]
        public async Task<IActionResult> SendArtworkRejectedByVendorEmail(string Template, string Servername, string ExtraUrl, string OrderNo, string Site, string varUserId, string Fullname, string varExistingFileID, string REJECTREASON)
        {
            Servername = Servername.Replace("%2F", "/");
            if (ExtraUrl == "null")
            {
                ExtraUrl = "";
            }
            string body = string.Empty;
            //string directoryPath = @"C:\Users\T3776844\OneDrive - Saint-Gobain\Documents\ct-customization-backend\CTSites\EmailTemplates";
            string directoryPath = @"D:\Websites\productcustomization.certainteed.com\API\EmailTemplates";
            string filePath = Path.Combine(directoryPath, Template + ".html");

            using (StreamReader reader = new StreamReader(filePath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("##SERVERNAME##", Servername);

            body = body.Replace("##EXTRAURL##", ExtraUrl);

            body = body.Replace("##DATE##", DateTime.Today.ToString());

            body = body.Replace("##DATETIME##", DateTime.Now.ToString());

            body = body.Replace("##CTORDERNUMBER##", OrderNo);

            body = body.Replace("##Site##", Site);
            body = body.Replace("##REJECTREASON##", REJECTREASON);

            body = body.Replace("##SITEURL##", "<a href=\"http://" + Servername + ExtraUrl + "\">http://" + Servername + ExtraUrl + "</a>");
            body = body.Replace("##AUTOLOGINURL##", "https://" + Servername + ExtraUrl + "/Index.asp?Action=SubmitForm&UserID=" + varUserId + "&OrderID=" + OrderNo);
            body = body.Replace("##FULLNAME##", Fullname);
            body = body.Replace("##SITEURL##", "<a href=\"http://" + Servername + ExtraUrl + "\">http://" + Servername + ExtraUrl + "</a>");

            body = body.Replace("##DOWNLOADURL##", "<a href=\"https://" + Servername + ExtraUrl + "/DownloadFile.asp?Vendor=True&Type=Artwork&ID=" + varExistingFileID + "\">http://" + Servername + ExtraUrl + "/DownloadFile.asp?Vendor=True&Type=Artwork&ID=" + varExistingFileID + "</a>");

            body = body.Replace("##COPYRIGHT##", DateTime.Now.Year.ToString());
            MailRequest request = new MailRequest();
            request.Subject = "Artwork Rejected for Order #" + OrderNo + " -- CertainTeed Roofing Customized " + Site;
            request.Body = body;
            //request.ToEmail = "creativeservices@saint-gobain.com";
            request.ToEmail = "ashwini.mane@saint-gobain.com";
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spGetVendorEmailAddress";
            var result = new Users();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@SiteId", Site);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        result.EmailAddress = reader["EmailAddress"].ToString();
                        request.ToEmail = request.ToEmail + ", " + result.EmailAddress;


                    }
                }

                con.Close();

                SendEmailAsync(request.ToEmail, request.Body, request.Subject);

                /*MailController mailController = new MailController(mailService);
                await mailController.SendMail(request);*/
                return Ok();
            }

        }

        [HttpPost]
        [Route("SendProofApprovedContinueOrderEmail/{Template}/{Servername}/{ExtraUrl}/{OrderNo}/{Site}/{varExistingFileID}")]
        public async Task<IActionResult> SendProofApprovedContinueOrderEmail(string Template, string Servername, string ExtraUrl, string OrderNo, string Site, string varExistingFileID)
        {
            Servername = Servername.Replace("%2F", "/");
            if (ExtraUrl == "null")
            {
                ExtraUrl = "";
            }
            string body = string.Empty;
            //string directoryPath = @"C:\Users\T3776844\OneDrive - Saint-Gobain\Documents\ct-customization-backend\CTSites\EmailTemplates";
            string directoryPath = @"D:\Websites\productcustomization.certainteed.com\API\EmailTemplates";
            string filePath = Path.Combine(directoryPath, Template + ".html");

            using (StreamReader reader = new StreamReader(filePath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("##SERVERNAME##", Servername);

            body = body.Replace("##EXTRAURL##", ExtraUrl);

            body = body.Replace("##DATE##", DateTime.Today.ToString());

            body = body.Replace("##DATETIME##", DateTime.Now.ToString());

            body = body.Replace("##CTORDERNUMBER##", OrderNo);

            body = body.Replace("##Site##", Site);


            body = body.Replace("##SITEURL##", "<a href=\"http://" + Servername + ExtraUrl + "\">http://" + Servername + ExtraUrl + "</a>");



            body = body.Replace("##DOWNLOADURL##", "<a href=\"https://" + Servername + ExtraUrl + "/DownloadFile.asp?Vendor=True&Type=Artwork&ID=" + varExistingFileID + "\">http://" + Servername + ExtraUrl + "/DownloadFile.asp?Vendor=True&Type=Artwork&ID=" + varExistingFileID + "</a>");

            body = body.Replace("##COPYRIGHT##", DateTime.Now.Year.ToString());
            MailRequest request = new MailRequest();
            request.Subject = "Proof Approved for Order #" + OrderNo + " -- CertainTeed Roofing Customized " + Site;
            request.Body = body;
            //request.ToEmail = "creativeservices@saint-gobain.com";
            request.ToEmail = "ashwini.mane@saint-gobain.com";
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spGetVendorEmailAddress";
            var result = new Users();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@SiteId", Site);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        result.EmailAddress = reader["EmailAddress"].ToString();
                        request.ToEmail = request.ToEmail + ", " + result.EmailAddress;


                    }
                }

                con.Close();

                SendEmailAsync(request.ToEmail, request.Body, request.Subject);

                /*MailController mailController = new MailController(mailService);
                await mailController.SendMail(request);*/
                return Ok();
            }

        }

        [HttpPost]
        [Route("SendProofRejectedByCustomerEmail/{Template}/{Servername}/{ExtraUrl}/{OrderNo}/{Site}/{varExistingFileID}/{REJECTREASON}")]
        public async Task<IActionResult> SendProofRejectedByCustomerEmail(string Template, string Servername, string ExtraUrl, string OrderNo, string Site, string varExistingFileID, string REJECTREASON)
        {
            Servername = Servername.Replace("%2F", "/");
            if (ExtraUrl == "null")
            {
                ExtraUrl = "";
            }
            string body = string.Empty;
            //string directoryPath = @"C:\Users\T3776844\OneDrive - Saint-Gobain\Documents\ct-customization-backend\CTSites\EmailTemplates";
            string directoryPath = @"D:\Websites\productcustomization.certainteed.com\API\EmailTemplates";
            string filePath = Path.Combine(directoryPath, Template + ".html");

            using (StreamReader reader = new StreamReader(filePath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("##SERVERNAME##", Servername);

            body = body.Replace("##EXTRAURL##", ExtraUrl);

            body = body.Replace("##DATE##", DateTime.Today.ToString());

            body = body.Replace("##DATETIME##", DateTime.Now.ToString());

            body = body.Replace("##CTORDERNUMBER##", OrderNo);

            body = body.Replace("##Site##", Site);
            body = body.Replace("##REJECTREASON##", REJECTREASON);

            body = body.Replace("##SITEURL##", "<a href=\"http://" + Servername + ExtraUrl + "\">http://" + Servername + ExtraUrl + "</a>");

            body = body.Replace("##DOWNLOADURL##", "<a href=\"https://" + Servername + ExtraUrl + "/DownloadFile.asp?Vendor=True&Type=Artwork&ID=" + varExistingFileID + "\">http://" + Servername + ExtraUrl + "/DownloadFile.asp?Vendor=True&Type=Artwork&ID=" + varExistingFileID + "</a>");

            body = body.Replace("##COPYRIGHT##", DateTime.Now.Year.ToString());
            MailRequest request = new MailRequest();
            request.Subject = "Proof Rejected for Order #" + OrderNo + " -- CertainTeed Roofing Customized " + Site;
            request.Body = body;
            // request.ToEmail = "creativeservices@saint-gobain.com";
            request.ToEmail = "ashwini.mane@saint-gobain.com";
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spGetVendorEmailAddress";
            var result = new Users();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
              
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@SiteId", Site);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        result.EmailAddress = reader["EmailAddress"].ToString();
                        request.ToEmail = request.ToEmail + ", " + result.EmailAddress;


                    }
                }

                con.Close();

                SendEmailAsync(request.ToEmail, request.Body, request.Subject);

                /*MailController mailController = new MailController(mailService);
                await mailController.SendMail(request);*/
                return Ok();
            }

        }

        [HttpPost]
        [Route("SendUpdateOrderStatusEmail/{Template}/{Servername}/{ExtraUrl}/{OrderNo}/{Site}/{MESSAGE}")]
        public async Task<IActionResult> SendUpdateOrderStatusEmail(string Template, string Servername, string ExtraUrl, string OrderNo, string Site, string MESSAGE)
        {
            Servername = Servername.Replace("%2F", "/");
            if (ExtraUrl == "null")
            {
                ExtraUrl = "";
            }
            string body = string.Empty;
            //string directoryPath = @"C:\Users\T3776844\OneDrive - Saint-Gobain\Documents\ct-customization-backend\CTSites\EmailTemplates";
            string directoryPath = @"D:\Websites\productcustomization.certainteed.com\API\EmailTemplates";
            string filePath = Path.Combine(directoryPath, Template + ".html");

            using (StreamReader reader = new StreamReader(filePath))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("##SERVERNAME##", Servername);

            body = body.Replace("##EXTRAURL##", ExtraUrl);

            body = body.Replace("##DATE##", DateTime.Today.ToString());

            body = body.Replace("##DATETIME##", DateTime.Now.ToString());

            body = body.Replace("##MESSAGE##", MESSAGE);

            body = body.Replace("##Site##", Site);


            body = body.Replace("##SITEURL##", "<a href=\"http://" + Servername + ExtraUrl + " / MyAdmin\">http://" + Servername + ExtraUrl + " / MyAdmin </a>");

            body = body.Replace("##COPYRIGHT##", DateTime.Now.Year.ToString());
            MailRequest request = new MailRequest();
            request.Subject = "Information Updated for Order #" + OrderNo + " -- CertainTeed Roofing Customized " + Site;
            request.Body = body;
            // request.ToEmail = "creativeservices@saint-gobain.com";
            request.ToEmail = "ashwini.mane@saint-gobain.com";
            SqlConnection con = new SqlConnection(_configuration.GetConnectionString("Dev_CTCustomization"));
            con.Open();
            string procedureName = "spGetCustomerServiceEmail";
            var result = new Users();
            using (SqlCommand command = new SqlCommand(procedureName, con))
            {
                command.Parameters.AddWithValue("@SiteId", Site);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {

                        result.EmailAddress = reader["EmailAddress"].ToString();
                        request.ToEmail = request.ToEmail + ", " + result.EmailAddress;


                    }
                }

                con.Close();

                SendEmailAsync(request.ToEmail, request.Body, request.Subject);

                /*MailController mailController = new MailController(mailService);
                await mailController.SendMail(request);*/
                return Ok();
            }

        }
    }

}
public class OrderStatusUpdateRequest
{
    public Guid OrderID { get; set; }
    public int StatusID { get; set; }
}
public class LogRequest
{
    public string? UserID { get; set; }
    public string IPAddress { get; set; }
    public string OrderID { get; set; }
    public string ActionType { get; set; }
    public string ActionDescription { get; set; }
    public string BrowserInfo { get; set; }
    public string Comments { get; set; }
    public string Status { get; set; }
}

