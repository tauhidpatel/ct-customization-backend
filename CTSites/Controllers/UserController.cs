﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net;
using System.Text;
using CTSites.Model;
using Microsoft.Extensions.Configuration;
using System.Net.Mail;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.IdentityModel.Tokens.Jwt;
using System.Text.RegularExpressions;
using System.Net.Http.Headers;

namespace CTSites.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        public readonly IConfiguration _configuration;
       

        public UserController(IConfiguration configuration)
        {
            _configuration = configuration;
           
        }
     
        string ApiToken = "SSWS 00tAlzVSue2FF8zmrLEsXMuwLMPDHoACPDvClPTqwE";
      
        [HttpPost]
        [Route("createuser")]
        public IActionResult createuser([FromForm] string FirstName, [FromForm] string LastName, [FromForm] string Email, [FromForm] string groupids, [FromForm] string mobilePhone, [FromForm] string organization)
        {
            string URL = _configuration.GetSection("URL").Value;
            Uri myUri = new Uri(URL + "/api/v1/users?activate=false");
            WebRequest myWebRequest = HttpWebRequest.Create(myUri);
            myWebRequest.ContentType = "application/json; charset=utf-8";
            // myWebRequest.Headers.Add("Cookie", "DT=DI0UJJh3K4bT5qmTNPIJ5zN_A; JSESSIONID=C2CADF27C9A05953B86300B2681F3E6F");
            myWebRequest.Headers.Add("Authorization", ApiToken);
            myWebRequest.Headers.Add("Access-Control-Allow-Origin", "*");
            myWebRequest.Method = "POST";
            Userprofile Userprofile = new Userprofile();
            Profile Profile = new Profile();
            Profile.firstName = FirstName;
            Profile.lastName = LastName;
            Profile.email = Email;
            Profile.login = Email;
            //    Profile.mobilePhone = mobilePhone;
            Profile.organization = organization;
            Userprofile.profile = Profile;
            Userprofile.groupIds = new[] { groupids };
            string json = JsonConvert.SerializeObject(Userprofile,
                                     Newtonsoft.Json.Formatting.None,
                                     new JsonSerializerSettings
                                     {
                                         NullValueHandling = NullValueHandling.Ignore
                                     });
            using (var streamWriter = new StreamWriter(myWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }
            try
            {
                var myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();

                StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
                //   Send(FirstName, LastName, Email);
                string pageContent = myStreamReader.ReadToEnd();
                var jsonObject = JsonConvert.DeserializeObject(myStreamReader.ReadToEnd());
                var values = new string[] { pageContent, myWebResponse.StatusCode.ToString() };
                return Ok(values);
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {

                        string text = reader.ReadToEnd();
                        var values = new string[] { text, httpResponse.StatusCode.ToString() };

                        return Ok(values);
                    }
                }
            }


        }
        
        [HttpGet]
        [Route("GetActivationToken/{userId}")]
        public IActionResult GetActivationToken(string userId)
        {
            string URL = _configuration.GetSection("URL").Value;
            var key = _configuration.GetSection("JwtKey").Value;

            var issuer = _configuration.GetSection("JwtIssuer").Value;

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));
            permClaims.Add(new Claim("userid", userId));

            //Create Security Token object by giving required parameters    GetActivationToken
            var token = new JwtSecurityToken(issuer, //Issure    
                            issuer,  //Audience    
                            permClaims,
                            expires: DateTime.Now.AddDays(6),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);
            return Ok(jwt_token.ToString());
        }
        [HttpPost]
        [Route("UpdateUser")]
        public IActionResult updateuser([FromForm] string userid, [FromForm] string FirstName, [FromForm] string LastName, [FromForm] string Email, [FromForm] string organization)
        {
            string URL = _configuration.GetSection("URL").Value;
            Uri myUri = new Uri(URL + "/api/v1/users/" + userid);
            WebRequest myWebRequest = HttpWebRequest.Create(myUri);
            myWebRequest.ContentType = "application/json; charset=utf-8";
            // myWebRequest.Headers.Add("Cookie", "DT=DI0UJJh3K4bT5qmTNPIJ5zN_A; JSESSIONID=C2CADF27C9A05953B86300B2681F3E6F");
            myWebRequest.Headers.Add("Authorization", ApiToken);
            myWebRequest.Headers.Add("Access-Control-Allow-Origin", "*");
            myWebRequest.Method = "POST";
            Userprofile1 Userprofile = new Userprofile1();
            Profile Profile = new Profile();
            Profile.firstName = FirstName;
            Profile.lastName = LastName;
            Profile.email = Email;
            Profile.login = Email;
            //   Profile.mobilePhone = mobilePhone;
            Profile.organization = organization;

            Userprofile.profile = Profile;

            string json = JsonConvert.SerializeObject(Userprofile,
                            Newtonsoft.Json.Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });
            using (var streamWriter = new StreamWriter(myWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }
            try
            {
                var myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();

                StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
                //   Send(FirstName, LastName, Email);
                string pageContent = myStreamReader.ReadToEnd();
                var jsonObject = JsonConvert.DeserializeObject(myStreamReader.ReadToEnd());
                var values = new string[] { pageContent, myWebResponse.StatusCode.ToString() };
                return Ok(values);
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {

                        string text = reader.ReadToEnd();
                        var values = new string[] { text, httpResponse.StatusCode.ToString() };

                        return Ok(values);
                    }
                }
            }


        }


        [HttpGet]
        [Route("CheckUserEmail/{Email}")]
        public IActionResult CheckUserEmailAsync(string Email)
        {
            string URL = _configuration.GetSection("URL").Value;
            // string status = await Refreshtoken(token);
            string values = "";
            //  var values = new string[] { };
            if (Email != null)
            {
                string URL1 = URL + "/api/v1/users?search=profile.login eq %22" + Email + "%22";
                Uri myUri = new Uri(URL1);
                WebRequest myWebRequest = WebRequest.Create(myUri);
                myWebRequest.ContentType = "application/json; charset=utf-8";
                // myWebRequest.Headers.Add("Cookie", "DT=DI0UJJh3K4bT5qmTNPIJ5zN_A; JSESSIONID=C2CADF27C9A05953B86300B2681F3E6F");
                myWebRequest.Headers.Add("Authorization", ApiToken);
                myWebRequest.Headers.Add("Access-Control-Allow-Origin", "*");
                myWebRequest.Method = "GET";

                try
                {
                    var myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                    Stream responseStream = myWebResponse.GetResponseStream();

                    StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
                    //  Send(FirstName, LastName, Email);
                    string pageContent = myStreamReader.ReadToEnd();
                    values = pageContent;
                  //  Json(values);
                    //  values = new string[] { pageContent, myWebResponse.StatusCode.ToString() };
                    //return new JsonpResult(values);
                }
                catch (WebException e)
                {
                    using (WebResponse response = e.Response)
                    {
                        HttpWebResponse httpResponse = (HttpWebResponse)response;
                        Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                        using (Stream data = response.GetResponseStream())
                        using (var reader = new StreamReader(data))
                        {

                            string text = reader.ReadToEnd();
                            values = text;
                            //values = new string[] { text, httpResponse.StatusCode.ToString() };

                           
                        }
                    }
                }
                return Ok(values);

            }
            else
            {
                 values = "Not Found";
                Ok(values);
            }


            return Ok(values);

        }


        [HttpGet]
        [Route("CheckUserEmailtest/{Email}")]
        public IActionResult CheckUserEmailtest(string Email)
        {
            string URL = _configuration.GetSection("URL").Value;
            // string status = await Refreshtoken(token);
            string values = "";
            //  var values = new string[] { };
           
                string URL1 = URL + "/api/v1/users?search=profile.login eq %22" + Email + "%22";

            //var client = new HttpClient();
            //var request = new HttpRequestMessage(HttpMethod.Get, URL1);
            //request.Headers.Add("Accept", "application/json");
            //request.Headers.Add("Authorization", ApiToken);
            //var content = new StringContent(string.Empty);
            //content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            //request.Content = content;
            //var response = await client.SendAsync(request);
            //response.EnsureSuccessStatusCode();
            //Console.WriteLine(await response.Content.ReadAsStringAsync());
            //return Ok(await response.Content.ReadAsStringAsync());
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            client.DefaultRequestHeaders.Add("Authorization", ApiToken);
            client.DefaultRequestHeaders.Add("Access-Control-Allow-Origin", "*");
            HttpResponseMessage response = client.GetAsync(URL1).Result;

            if (response.IsSuccessStatusCode)
            {
                string page = response.Content.ReadAsStringAsync().Result;
                return Ok(page + "Successfully load page");
            }
            else
            {
                return Ok("Invalid Page url requested");
            }

        }

        [HttpPost]
        [Route("Activateuser/{Userid}")]
        public IActionResult Activateuser(string Userid)
        {
            string URL = _configuration.GetSection("URL").Value;
            Uri myUri = new Uri(URL + "/api/v1/users/" + Userid + "/lifecycle/activate?sendEmail=false");
            WebRequest myWebRequest = HttpWebRequest.Create(myUri);
            myWebRequest.ContentType = "application/json; charset=utf-8";
            // myWebRequest.Headers.Add("Cookie", "DT=DI0UJJh3K4bT5qmTNPIJ5zN_A; JSESSIONID=C2CADF27C9A05953B86300B2681F3E6F");
            myWebRequest.Headers.Add("Authorization", ApiToken);
            myWebRequest.Headers.Add("Access-Control-Allow-Origin", "*");
            myWebRequest.Method = "POST";



            using (var streamWriter = new StreamWriter(myWebRequest.GetRequestStream()))
            {
                //streamWriter.Write(json);
            }
            try
            {
                var myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();

                StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
                //  Send(FirstName, LastName, Email);
                string pageContent = myStreamReader.ReadToEnd();
                var values = new string[] { pageContent, myWebResponse.StatusCode.ToString() };
                return Ok(values);
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {

                        string text = reader.ReadToEnd();
                        var values = new string[] { text, httpResponse.StatusCode.ToString() };

                        return Ok(values);
                    }
                }
            }


        }


        [HttpPost]
        [Route("Send/{FirstName}/{LastName}/{Email}/{Token}/{User}/{WebsiteURL}")]
        public IActionResult Send(string FirstName, string LastName, string Email, string Token,string User, string WebsiteURL)
        {
            string Message = "";
            WebsiteURL = Regex.Replace(WebsiteURL, "%2F", "/");
            string URL = WebsiteURL + "/ChangePassword/" + User+"/"+Token;
           
            MailMessage msg = new MailMessage();
            System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient();
            msg.From = new MailAddress("DoNotReply@saint-gobain.com", "No-Reply", System.Text.Encoding.UTF8);
            msg.Subject = "(Action Required) Saint-Gobain/CertainTeed User Account Created Confirmation";//Message header 
            msg.SubjectEncoding = Encoding.UTF8;//Mail title code 
            msg.IsBodyHtml = true;

            msg.Body = "Hi " + FirstName + " " + LastName + ", < br >< br > Saint - Gobain / CertainTeed uses a single sign-on service to manage account information for some of our applications.This allows you to have the same account credentials across our applications.You must set your password by < a href = '" + URL + "' > click here </ a > or by copying & pasting the following link into your web browser’s address bar: " + URL + ".< br >< br > Thank you,< br > Saint - Gobain / CertainTeed";;
            msg.To.Add(Email);
            msg.BodyEncoding = Encoding.UTF8;//Message code 
            msg.IsBodyHtml = true;//Whether the HTML mail 
            msg.Priority = MailPriority.High;//Priority mail
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls12;
            //     SmtpServer.Port = 587;
            client.Credentials = new System.Net.NetworkCredential("", "password");
            //client.Host = Get_AppSettings("smtpKey");
            client.Host = "localhost";
            object userState = msg;
            try
            {
                client.Send(msg);
                Message = "success";
                //  return true;
            }
            catch (Exception err)
            {
                Message = err.InnerException.ToString();
                System.Diagnostics.Debug.WriteLine("MAIL ERROR: " + err.Message.ToString());
            }

            return Ok(Message);
        }


        [HttpPost]
        [Route("AddUserGroup/{groupid}/{userid}")]
        public IActionResult AddUserGroup(string groupid, string userid)
        {

            string URL = _configuration.GetSection("URL").Value;
            Uri myUri = new Uri(URL + "/api/v1/groups/" + groupid + "/users/" + userid);
            WebRequest myWebRequest = HttpWebRequest.Create(myUri);
            myWebRequest.ContentType = "application/json; charset=utf-8";
            // myWebRequest.Headers.Add("Cookie", "DT=DI0UJJh3K4bT5qmTNPIJ5zN_A; JSESSIONID=C2CADF27C9A05953B86300B2681F3E6F");
            myWebRequest.Headers.Add("Authorization", ApiToken);
            myWebRequest.Headers.Add("Access-Control-Allow-Origin", "*");
            myWebRequest.Method = "PUT";

            using (var streamWriter = new StreamWriter(myWebRequest.GetRequestStream()))
            {

            }
            try
            {
                var myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();

                StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
                //  Send(FirstName, LastName, Email);
                string pageContent = myStreamReader.ReadToEnd();
                var values = new string[] { pageContent, myWebResponse.StatusCode.ToString() };
                return Ok(values);
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {

                        string text = reader.ReadToEnd();
                        var values = new string[] { text, httpResponse.StatusCode.ToString() };

                        return Ok(values);
                    }
                }
            }


        }


        [HttpPost]
        [Route("ForgotPasswordSend/{FirstName}/{LastName}/{Email}/{User}/{WebsiteURL}")]
        public IActionResult ForgotPasswordSend(string FirstName, string LastName, string Email, string User, string WebsiteURL)
        {
            string Message = "";

            WebsiteURL = Regex.Replace(WebsiteURL, "%2F", "/");
            string URL = WebsiteURL + "/Resetpassword/" + User ;
            MailMessage msg = new MailMessage();
            SmtpClient client = new SmtpClient();
            msg.From = new MailAddress("DoNotReply@saint-gobain.com", "No-Reply", System.Text.Encoding.UTF8);
            msg.Subject = "(Action Required) Saint-Gobain/CertainTeed User Account Password Reset";//Message header 
            msg.SubjectEncoding = Encoding.UTF8;//Mail title code 
            msg.IsBodyHtml = true;

            msg.Body = "Hi " + FirstName + " " + LastName + ", <br><br>We have received a request to reset your password.  You can reset your password by <a href='" + URL + "'>click here </a> or by copying & pasting the following link into your web browser’s address bar: " + URL + " Saint-Gobain/CertainTeed uses a single sign-on service to manage account information for some of our applications.  This allows you to have the same account credentials across our applications.  If you did not initiate this request then please ignore this email.<br><br> Thank you,<br> Saint-Gobain/CertainTeed";
            msg.To.Add(Email);
            msg.BodyEncoding = Encoding.UTF8;//Message code 
            msg.IsBodyHtml = true;//Whether the HTML mail 
            msg.Priority = MailPriority.High;//Priority mail

            client.Port = 25;
            client.Credentials = new System.Net.NetworkCredential("", "password");
            //client.Host = Get_AppSettings("smtpKey");
            client.Host = "localhost";
            object userState = msg;
            try
            {
                client.Send(msg);
                Message = "success";
                //  return true;
            }
            catch (Exception err)
            {
                Message = err.InnerException.ToString();
                System.Diagnostics.Debug.WriteLine("MAIL ERROR: " + err.Message.ToString());
            }

            return  Ok(Message);
        }
        [HttpGet]
        [Route("VerifyToken/{Token}")]
        public IActionResult VerifyToken(string Token)
        {
            string URL = _configuration.GetSection("URL").Value;
            Uri myUri = new Uri(URL + "/api/v1/authn/recovery/token");
            WebRequest myWebRequest = HttpWebRequest.Create(myUri);
            myWebRequest.ContentType = "application/json; charset=utf-8";
            // myWebRequest.Headers.Add("Cookie", "DT=DI0UJJh3K4bT5qmTNPIJ5zN_A; JSESSIONID=C2CADF27C9A05953B86300B2681F3E6F");
            // myWebRequest.Headers.Add("Authorization", ApiToken);
            myWebRequest.Headers.Add("Access-Control-Allow-Origin", "*");
            myWebRequest.Method = "POST";

            Token token = new Token();
            token.recoveryToken = Token;

            string json = JsonConvert.SerializeObject(token);
            using (var streamWriter = new StreamWriter(myWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }
            try
            {
                var myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();

                StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
                //  Send(FirstName, LastName, Email);
                string pageContent = myStreamReader.ReadToEnd();
                var values = new string[] { pageContent, myWebResponse.StatusCode.ToString() };
                return Ok(values);
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {

                        string text = reader.ReadToEnd();
                        var values = new string[] { text, httpResponse.StatusCode.ToString() };

                        return Ok(values);
                    }
                }
            }


        }
        [HttpGet]
        [Route("SetPassword/{User}/{Password}")]
        public IActionResult SetPassword(string User, string Password)
        {
            string URL = _configuration.GetSection("URL").Value;
            Uri myUri = new Uri(URL + "/api/v1/users/" + User);
            WebRequest myWebRequest = HttpWebRequest.Create(myUri);
            myWebRequest.ContentType = "application/json; charset=utf-8";
            // myWebRequest.Headers.Add("Cookie", "DT=DI0UJJh3K4bT5qmTNPIJ5zN_A; JSESSIONID=C2CADF27C9A05953B86300B2681F3E6F");
            myWebRequest.Headers.Add("Authorization", ApiToken);
            myWebRequest.Headers.Add("Access-Control-Allow-Origin", "*");
            myWebRequest.Method = "PUT";
            credentials credentials = new credentials();
            credentials.password = Password;
            SetPassword sp = new SetPassword();
            sp.credentials = credentials;


            string json = JsonConvert.SerializeObject(sp);
            using (var streamWriter = new StreamWriter(myWebRequest.GetRequestStream()))
            {
                streamWriter.Write(json);
            }
            try
            {
                var myWebResponse = (HttpWebResponse)myWebRequest.GetResponse();
                Stream responseStream = myWebResponse.GetResponseStream();

                StreamReader myStreamReader = new StreamReader(responseStream, Encoding.Default);
                //  Send(FirstName, LastName, Email);
                string pageContent = myStreamReader.ReadToEnd();
                var values = new string[] { pageContent, myWebResponse.StatusCode.ToString() };
                return  Ok(values); ;
            }
            catch (WebException e)
            {
                using (WebResponse response = e.Response)
                {
                    HttpWebResponse httpResponse = (HttpWebResponse)response;
                    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                    using (Stream data = response.GetResponseStream())
                    using (var reader = new StreamReader(data))
                    {

                        string text = reader.ReadToEnd();
                        var values = new string[] { text, httpResponse.StatusCode.ToString() };

                        return Ok(values);
                    }
                }
            }


        }
    }
}
