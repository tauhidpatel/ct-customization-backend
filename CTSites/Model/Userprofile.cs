﻿namespace CTSites.Model
{
    public class Userprofile
    {
        public Profile profile { get; set; }
        public string[] groupIds { get; set; }
        public credentials credentials { get; set; }
    }

    public class Profile

    {

        public string firstName { get; set; }
        public string lastName { get; set; }

        public string email { get; set; }
        public string login { get; set; }
        public string mobilePhone { get; set; }
        public string organization { get; set; }
        //    public string phoneNumber { get; set; }

    }
    public class credentials
    {
        public string password { get; set; }
        public provider provider { get; set; }
    }
    public class provider
    {
        public string type { get; set; }
        public string name { get; set; }
    }
    public class Userprofile1
    {
        public Profile profile { get; set; }

    }
    public class Token
    {
        public string recoveryToken;
    }

    public class SetPassword
    {
        public credentials credentials { get; set; }
    }

}
