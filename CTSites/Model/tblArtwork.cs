﻿namespace CTSites.Model
{
    public class tblArtwork
    {
        public int ProxyID { get; set; }
        public string ArtworkID { get; set; }
        public string UserId { get; set; }
        public string FileName { get; set; }
        public string OrigFileName { get; set; }
        public string DateAdded { get; set; }
        public string PMSColors { get; set; }

        public string OrderId { get; set; }

    }
}
