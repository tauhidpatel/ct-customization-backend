﻿namespace CTSites.Model
{
    public class tblProofs
    {
        public int ProxyID { get; set; }
        public string ProofID { get; set; }
        public string UserId { get; set; }
        public string FileName { get; set; }
        public string OrigFileName { get; set; }
        public string DateAdded { get; set; }
        public string CustomerComments { get; set; }
        public string CustomerStatus { get; set; }
        public string PMSColors { get; set; }
        public string OrderId { get; set; }

    }
}
