﻿namespace CTSites.Model
{
    public class Customer
    {
       public int ProxyID { get; set; }
       public string Userid { get; set; }
       public string OrderID { get; set; }
       public string OrderByAccountNumber { get; set; }
       public string ShipToAccountNumber { get; set; }
       public string FullName { get; set; }
       public string EmailAddress { get; set; } 
       public string DateAdded { get; set; }
       public string DateLastUsed { get; set; }
       public string EnteredBy {  get; set; }
       public string IsPrimary { get; set; }
    }
}
