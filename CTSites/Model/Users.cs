﻿namespace CTSites.Model
{
    public class Users
    {
        public int UserID { get; set; }
        public int Siteid { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string ZipCode { get; set; }
        public string Phone { get; set; }
        public string EmailAddress { get; set; }
        public string Status { get; set; }

        public string DateCreated { get; set; }
        public string DateUpdated { get; set; }
        public string SecurityLevel { get; set; }
        public string SGID { get; set; }
        public string oktauserid { get; set; }
    }
}
