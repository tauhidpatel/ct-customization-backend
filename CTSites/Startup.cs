﻿using CTSites.Data;
using CTSites.Services;
using CTSites.Settings;
using Microsoft.EntityFrameworkCore;

namespace CTSites
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
       
       // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
       
        
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<MailSettings>(Configuration.GetSection("MailSettings"));
            services.AddTransient<IMailService, Services.MailService>();
            services.AddControllers();
            services.AddMvc();
            string conStr = this.Configuration.GetConnectionString("Dev_CTCustomization");
            services.AddDbContext<OrderContext>(options => options.UseSqlServer(conStr));

        }
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }

    }
}
