﻿using CTSites.Model;
using System.Threading.Tasks;
namespace CTSites.Services
{
    public interface IMailService
    {
        Task SendEmailAsync(MailRequest mailRequest);
    }
}
